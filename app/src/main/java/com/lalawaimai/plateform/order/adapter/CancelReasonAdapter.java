package com.lalawaimai.plateform.order.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.lalawaimai.palteform.baselib.base.BaseMineAdapter;
import com.lalawaimai.plateform.R;
import java.util.List;

public class CancelReasonAdapter extends BaseMineAdapter<String>{

    private boolean[] listCheck;

    /**
     * 如果这个不止一个列表，就在子类的Adapter中重写一个set方法放进来
     *
     * @param activity
     * @param list
     */
    public CancelReasonAdapter(Activity activity, List<String> list) {
        super(activity, list);
    }

    @Override
    protected Object getHolderChild() {
        return new MineHolder();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_cancel_reason;
    }

    @Override
    protected void initHolderWidge(View convertView, Object object) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.reason_name = convertView.findViewById(R.id.reason_name);
        mineHolder.iv_check = convertView.findViewById(R.id.iv_check);
    }

    @Override
    protected void initHolderData(Object object, int position) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.reason_name.setText(list.get(position));
        if (listCheck[position]){
            mineHolder.iv_check.setVisibility(View.VISIBLE);
        }else{
            mineHolder.iv_check.setVisibility(View.INVISIBLE);
        }
    }

    public class MineHolder{
        public TextView reason_name;
        public ImageView iv_check;
    }

    public void setListCheck(boolean[] listCheck) {
        this.listCheck = listCheck;
    }
}
