package com.lalawaimai.plateform.bean;

/**
 * 序列号返回实体类
 * Created by WaterWood on 2018/6/8.
 */
public class SerialNumberBean {

    private MessageBean message;
    private String redirect;
    private String type;

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class MessageBean {

        private int resultCode;
        private String resultMessage;
        private DataBean data;

        public int getResultCode() {
            return resultCode;
        }

        public void setResultCode(int resultCode) {
            this.resultCode = resultCode;
        }

        public String getResultMessage() {
            return resultMessage;
        }

        public void setResultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {

            private int uniacid;
            private String serial_sn;
            private int port;
            private String url;

            public int getUniacid() {
                return uniacid;
            }

            public void setUniacid(int uniacid) {
                this.uniacid = uniacid;
            }

            public String getSerial_sn() {
                return serial_sn;
            }

            public void setSerial_sn(String serial_sn) {
                this.serial_sn = serial_sn;
            }

            public int getPort() {
                return port;
            }

            public void setPort(int port) {
                this.port = port;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }
    }
}
