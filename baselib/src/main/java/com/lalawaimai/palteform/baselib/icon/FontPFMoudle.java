package com.lalawaimai.palteform.baselib.icon;

import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconFontDescriptor;

/**
 * 阿里巴巴矢量图导入
 * Created by WaterWood on 2018/6/4.
 */
public class FontPFMoudle implements IconFontDescriptor{
    @Override
    public String ttfFileName() {
        return "iconfont.ttf";
    }

    @Override
    public Icon[] characters() {
        return EcIcons.values();
    }
}
