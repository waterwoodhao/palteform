package com.lalawaimai.plateform.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lalawaimai.palteform.baselib.base.BaseCommonActivity;
import com.lalawaimai.palteform.baselib.http.CommonBean;
import com.lalawaimai.palteform.baselib.http.DisposeDataListener;
import com.lalawaimai.palteform.baselib.http.OkHttpException;
import com.lalawaimai.palteform.baselib.http.RequestWebInfo;
import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.SerialNumberBean;
import com.lalawaimai.plateform.constant.Constant;
import com.lalawaimai.plateform.http.WebUrl;

import java.util.ArrayList;
import java.util.List;

/**
 * 序列号输入页面
 * Created by WaterWood on 2018/6/7.
 */
public class SerialnumberActivity extends BaseCommonActivity implements View.OnClickListener {

    private View status_bar;
    private LinearLayout ll_back;
    private TextView tv_title;
    private EditText et_serial;
    private Button bt_commit;
    private ProgressDialog dialogWeb;//网络加载等待框

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_serial_number;
    }

    @Override
    protected void initWidget() {
        status_bar = findViewById(R.id.status_bar);
        ll_back = findViewById(R.id.ll_back);
        tv_title = findViewById(R.id.tv_title);
        et_serial = findViewById(R.id.et_serial);
        bt_commit = findViewById(R.id.bt_commit);
        ll_back.setVisibility(View.GONE);
        bt_commit.setOnClickListener(this);
        setStatusBarSize(this, status_bar);
        tv_title.setText("序列号");
        dialogWeb = CommonUtil.showProgressDialog(this, "正在加载");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_commit:
                commit();
                break;
        }
    }

    /**
     * 执行提交
     */
    private void commit() {
        String serialStr = et_serial.getText().toString().trim();
        if (!NullUtil.isStringEmpty(serialStr)) {
            dialogWeb.show();
            String url = WebUrl.REPLY_SERIALNUM;
            Class<?> clazz = SerialNumberBean.class;
            final List<String> list = new ArrayList<>();
            list.add("serial_sn");
            list.add("type");
            list.add("client");
            new RequestWebInfo(new DisposeDataListener() {
                @Override
                public void onSuccess(Object responseObj) {
                    dialogWeb.dismiss();
                    SerialNumberBean serialNumberBean = (SerialNumberBean) responseObj;
                    //保存相关内容
                    CommonUtil.saveData(SerialnumberActivity.this, Constant.UNIACID,serialNumberBean.getMessage().getData().getUniacid(),Constant.INT);
                    CommonUtil.saveData(SerialnumberActivity.this,Constant.URL,serialNumberBean.getMessage().getData().getUrl(),Constant.STRING);
                    Intent intent = new Intent(SerialnumberActivity.this, LoginActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(Object reasonObj) {
                    dialogWeb.dismiss();
                    OkHttpException okHttpException = (OkHttpException) reasonObj;
                    String msg = okHttpException.getEmsg().toString();
                    if (msg.contains("java.net.ConnectException")) {
                        CommonUtil.showToast(SerialnumberActivity.this, "无法连接服务器，请检查您的网络");
                    } else {
                        CommonUtil.showToast(SerialnumberActivity.this, msg);
                    }
                }
            }).requestWeb(url, clazz, list, serialStr, "plateform", "android");
        } else {
            CommonUtil.showToast(this, "请输入序列号");
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
