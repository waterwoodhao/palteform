package com.lalawaimai.plateform.bean;

/**
 * 商铺实体--详细参数
 * Created by WaterWood on 2018/6/4.
 */
public class ShopListEntity {
    private String id;
    private String title;
    private String address;
    private String telephone;
    private int is_rest;
    private String status;
    private String status_cn;
    private String logo;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public int getIs_rest() {
        return is_rest;
    }

    public void setIs_rest(int is_rest) {
        this.is_rest = is_rest;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_cn() {
        return status_cn;
    }

    public void setStatus_cn(String status_cn) {
        this.status_cn = status_cn;
    }
}
