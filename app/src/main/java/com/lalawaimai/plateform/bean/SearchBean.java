package com.lalawaimai.plateform.bean;

import java.util.List;

/**
 * 搜索页面初始化实体
 * Created by WaterWood on 2018/6/2.
 */
public class SearchBean {

    private MessageBean message;
    private String redirect;
    private String type;

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class MessageBean {

        private int resultCode;
        private String resultMessage;
        private DataBean data;

        public int getResultCode() {
            return resultCode;
        }

        public void setResultCode(int resultCode) {
            this.resultCode = resultCode;
        }

        public String getResultMessage() {
            return resultMessage;
        }

        public void setResultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            private List<SearchEntity> deliveryers;
            private List<SearchEntity> stores;
            private List<SearchEntity> agents;

            public List<SearchEntity> getDeliveryers() {
                return deliveryers;
            }

            public void setDeliveryers(List<SearchEntity> deliveryers) {
                this.deliveryers = deliveryers;
            }

            public List<SearchEntity> getStores() {
                return stores;
            }

            public void setStores(List<SearchEntity> stores) {
                this.stores = stores;
            }

            public List<SearchEntity> getAgents() {
                return agents;
            }

            public void setAgents(List<SearchEntity> agents) {
                this.agents = agents;
            }
        }
    }
}
