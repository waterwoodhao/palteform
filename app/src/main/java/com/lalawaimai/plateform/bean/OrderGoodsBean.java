package com.lalawaimai.plateform.bean;

/**
 * 订单详情页商品信息
 * Created by WaterWood on 2018/6/8.
 */
public class OrderGoodsBean {

    private String id;
    private String oid;
    private String uniacid;
    private String sid;
    private String uid;
    private String goods_id;
    private String goods_cid;
    private String option_id;
    private String goods_num;
    private String goods_discount_num;
    private String goods_title;
    private String goods_unit_price;
    private String goods_price;
    private String goods_original_price;
    private String bargain_id;
    private String total_update_status;
    private String print_label;
    private String status;
    private String order_plateform;
    private String addtime;
    private String goods_category_title;
    private String stat_year;
    private String stat_month;
    private String stat_week;
    private String stat_day;
    private String agentid;
    private String goods_number;
    private String thumb;
    private int activity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getUniacid() {
        return uniacid;
    }

    public void setUniacid(String uniacid) {
        this.uniacid = uniacid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_cid() {
        return goods_cid;
    }

    public void setGoods_cid(String goods_cid) {
        this.goods_cid = goods_cid;
    }

    public String getOption_id() {
        return option_id;
    }

    public void setOption_id(String option_id) {
        this.option_id = option_id;
    }

    public String getGoods_num() {
        return goods_num;
    }

    public void setGoods_num(String goods_num) {
        this.goods_num = goods_num;
    }

    public String getGoods_discount_num() {
        return goods_discount_num;
    }

    public void setGoods_discount_num(String goods_discount_num) {
        this.goods_discount_num = goods_discount_num;
    }

    public String getGoods_title() {
        return goods_title;
    }

    public void setGoods_title(String goods_title) {
        this.goods_title = goods_title;
    }

    public String getGoods_unit_price() {
        return goods_unit_price;
    }

    public void setGoods_unit_price(String goods_unit_price) {
        this.goods_unit_price = goods_unit_price;
    }

    public String getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(String goods_price) {
        this.goods_price = goods_price;
    }

    public String getGoods_original_price() {
        return goods_original_price;
    }

    public void setGoods_original_price(String goods_original_price) {
        this.goods_original_price = goods_original_price;
    }

    public String getBargain_id() {
        return bargain_id;
    }

    public void setBargain_id(String bargain_id) {
        this.bargain_id = bargain_id;
    }

    public String getTotal_update_status() {
        return total_update_status;
    }

    public void setTotal_update_status(String total_update_status) {
        this.total_update_status = total_update_status;
    }

    public String getPrint_label() {
        return print_label;
    }

    public void setPrint_label(String print_label) {
        this.print_label = print_label;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrder_plateform() {
        return order_plateform;
    }

    public void setOrder_plateform(String order_plateform) {
        this.order_plateform = order_plateform;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getGoods_category_title() {
        return goods_category_title;
    }

    public void setGoods_category_title(String goods_category_title) {
        this.goods_category_title = goods_category_title;
    }

    public String getStat_year() {
        return stat_year;
    }

    public void setStat_year(String stat_year) {
        this.stat_year = stat_year;
    }

    public String getStat_month() {
        return stat_month;
    }

    public void setStat_month(String stat_month) {
        this.stat_month = stat_month;
    }

    public String getStat_week() {
        return stat_week;
    }

    public void setStat_week(String stat_week) {
        this.stat_week = stat_week;
    }

    public String getStat_day() {
        return stat_day;
    }

    public void setStat_day(String stat_day) {
        this.stat_day = stat_day;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public String getGoods_number() {
        return goods_number;
    }

    public void setGoods_number(String goods_number) {
        this.goods_number = goods_number;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }
}
