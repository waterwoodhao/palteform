package com.lalawaimai.plateform.shop.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.lalawaimai.palteform.baselib.base.BaseActivity;
import com.lalawaimai.palteform.baselib.base.BaseCommonActivity;
import com.lalawaimai.palteform.baselib.http.DisposeDataListener;
import com.lalawaimai.palteform.baselib.http.OkHttpException;
import com.lalawaimai.palteform.baselib.http.RequestWebInfo;
import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.ShopListBean;
import com.lalawaimai.plateform.bean.ShopListEntity;
import com.lalawaimai.plateform.constant.Constant;
import com.lalawaimai.plateform.http.WebUrl;
import com.lalawaimai.plateform.shop.adapter.ShopListAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索店铺
 * Created by WaterWood on 2018/6/4.
 */
public class SearchShopListActivity extends BaseCommonActivity implements View.OnClickListener{

    private View status_bar;
    private LinearLayout ll_back;
    private TextView tv_title;
    private EditText et_keyword;
    private int page = 1;
    private SmartRefreshLayout refreshLayout;
    private List<ShopListEntity> listShop;
    private ShopListAdapter mAdapter;
    private ListView lv_order;
    private final int MAX_PAGE = 20;
    private RelativeLayout rl_empty;

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_search_shoplist;
    }

    @Override
    protected void initWidget() {
        status_bar = findViewById(R.id.status_bar);
        ll_back = findViewById(R.id.ll_back);
        tv_title = findViewById(R.id.tv_title);
        et_keyword = findViewById(R.id.et_keyword);
        refreshLayout = findViewById(R.id.refreshLayout);
        lv_order = findViewById(R.id.lv_order);
        rl_empty = findViewById(R.id.rl_empty);
        ll_back.setOnClickListener(this);
        setStatusBarSize(this,status_bar);
        tv_title.setText("店铺搜索结果");
        et_keyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    if (NullUtil.isStringEmpty(et_keyword.getText().toString().trim())){
                        CommonUtil.showToast(SearchShopListActivity.this,"请输入要搜索的关键字");
                    }else {
                        page = 1;
                        searchShops();
                    }
                }
                return false;
            }
        });
        listShop = new ArrayList<>();
        mAdapter = new ShopListAdapter(this,listShop);
        lv_order.setAdapter(mAdapter);
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 1;
                searchShops();
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                page++;
                searchShops();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_back:
                finish();
                break;
        }
    }

    /**
     * 搜索请求接口
     */
    private void searchShops(){
        String url = WebUrl.GET_SHOP_LIST;
        Class<?> clazz = ShopListBean.class;
        List<String> list = new ArrayList<>();
        list.add("token");
        list.add("page");
        list.add("keyword");
        new RequestWebInfo(new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                ShopListBean shopListBean = (ShopListBean) responseObj;
                if (page == 1) {
                    refreshLayout.finishRefresh();
                    //刷新操作
                    List<ShopListEntity> list = shopListBean.getMessage().getData().getStores();
                    listShop.clear();
                    if (!NullUtil.isListEmpty(list)) {
                        //有东西
                        listShop.addAll(list);
                        //如果第一页就小于二十条，直接禁止加载更多
                        if (list.size() < MAX_PAGE) {
                            refreshLayout.setEnableLoadMore(false);
                        } else {
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                        rl_empty.setVisibility(View.GONE);
                    } else {
                        //没有东西，显示空页面，隐藏列表
                        rl_empty.setVisibility(View.VISIBLE);
                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    //加载更多
                    refreshLayout.finishLoadMore();
                    List<ShopListEntity> list = shopListBean.getMessage().getData().getStores();
                    if (!NullUtil.isListEmpty(list)) {
                        //有东西
                        listShop.addAll(list);
                        //如果这一页小于这么多，直接禁止加载更多
                        if (list.size() < MAX_PAGE) {
                            refreshLayout.setEnableLoadMore(false);
                        } else {
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                        rl_empty.setVisibility(View.GONE);
                    } else {
                        //没东西
                        CommonUtil.showToast(SearchShopListActivity.this, "已经加载到底了");
                        refreshLayout.setEnableLoadMore(false);
                    }
                }
            }

            @Override
            public void onFailure(Object reasonObj) {
                page--;
                if (page<1){
                    page = 1;
                }
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
                OkHttpException okHttpException = (OkHttpException) reasonObj;
                String msg = okHttpException.getEmsg().toString();
                if (msg.contains("java.net.ConnectException")) {
                    CommonUtil.showToast(SearchShopListActivity.this, "无法连接服务器，请检查您的网络");
                }
            }
        }).requestWeb(url, clazz, list, (String) CommonUtil.readData(this, Constant.TOKEN,Constant.STRING),page + "",et_keyword.getText().toString().trim());
    }
}
