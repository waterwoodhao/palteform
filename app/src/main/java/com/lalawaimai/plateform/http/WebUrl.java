package com.lalawaimai.plateform.http;

import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.application.MyApplication;
import com.lalawaimai.plateform.constant.Constant;

/**
 * 网址集合类
 * Created by WaterWood on 2018/5/31.
 */
public class WebUrl {
    private static final String WEB_URL_HEAD = "http://192.168.0.109/we7";//基础地址
    private static final String I = "1";

    public static final String GET_ORDER_LIST = getUrl("order", "takeout", "");//订单列表
    public static final String GET_ORDER_DETAIL = getUrl("order", "takeout", "detail");//订单详情
    public static final String REQUEST_BUTTON = getUrl("order","takeout","status");//列表中按钮的请求
    public static final String CANCEL_REASON = getUrl("order","takeout","cancel_reason");//取消订单原因
    public static final String SEARCH_INIT = getUrl("order","takeout","order_search");//搜索初始化
    public static final String GET_SHOP_LIST = getUrl("store","index","");//店铺列表
    public static final String DIAPATCH = getUrl("order","takeout","analyse");//调度接口
    public static final String DISPATCH_TO_DELIVERYER = getUrl("order","takeout","dispatch");//外卖订单调度后分配配送员
    public static final String REPLY_SERIALNUM = "http://up.hao071.com/app/index.php?i=1&c=entry&do=serial&m=tiny_manage";//序列号申请接口
    public static final String LOGIN = getUrl("auth","login","");//登录接口

    private static String getUrl(String acStr, String opStr, String taStr) {
        if (NullUtil.isStringEmpty(taStr)) {
            return CommonUtil.readData(MyApplication.getContext(),Constant.URL,Constant.STRING) + "/app/index.php?i=" + CommonUtil.readData(MyApplication.getContext(), Constant.UNIACID,Constant.INT) + "&c=entry&ctrl=plateform&ac=" + acStr + "&op=" + opStr + "&do=api&m=we7_wmall";
        } else {
            return CommonUtil.readData(MyApplication.getContext(),Constant.URL,Constant.STRING) + "/app/index.php?i=" + CommonUtil.readData(MyApplication.getContext(), Constant.UNIACID,Constant.INT) + "&c=entry&ctrl=plateform&ac=" + acStr + "&op=" + opStr + "&ta=" + taStr + "&do=api&m=we7_wmall";
        }
    }
}
