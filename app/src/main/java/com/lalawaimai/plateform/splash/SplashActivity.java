package com.lalawaimai.plateform.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ImageView;

import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.constant.Constant;
import com.lalawaimai.plateform.home.HomeActivity;
import com.lalawaimai.plateform.login.LoginActivity;
import com.lalawaimai.plateform.login.SerialnumberActivity;

/**
 * 闪屏页启动图
 * Created by WaterWood on 2018/5/30.
 */
public class SplashActivity extends Activity{

    private ImageView iv_splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //初始化控件
        iv_splash = findViewById(R.id.iv_splash);
        // TODO: 2018/5/30 这里是为控件加载图片，先加载一个固定图，以后肯定要加载非固定图
        // TODO: 2018/5/30 这里的默认图片要求有一个
        // TODO: 2018/5/30 这里有可能要做缓存，不能直接使用
//        String picUrl = "https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1527645677&di=6f35b47600c29dd424bbcd92e2464d48&src=http://img.zcool.cn/community/01386a598937b70000002129b851af.jpg@1280w_1l_2o_100sh.png";
//        Glide.with(this).load(picUrl).placeholder(R.mipmap.bg_splash).error(R.mipmap.bg_splash).into(iv_splash);
        iv_splash.setImageResource(R.mipmap.ic_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if ((int)CommonUtil.readData(SplashActivity.this, Constant.UNIACID,Constant.INT) == 0){
                    //跳转到序列号页面
                    Intent intent = new Intent(SplashActivity.this,SerialnumberActivity.class);
                    startActivity(intent);
                    finish();
                }else if (NullUtil.isStringEmpty((String) CommonUtil.readData(SplashActivity.this,Constant.TOKEN,Constant.STRING))){
                    //跳转到登录
                    Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    //跳转到主页
                    Intent intent = new Intent(SplashActivity.this,HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 2000);
    }
}
