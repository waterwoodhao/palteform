package com.lalawaimai.palteform.baselib.http;

/**
 * 回调信息监听接口
 * Created by WaterWood on 2018/5/29.
 */
public interface DisposeDataListener {
    //请求成功回调事件处理
    public void onSuccess(Object responseObj);
    //请求失败回调事件处理
    public void onFailure(Object reasonObj);
}
