package com.lalawaimai.plateform.order.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lalawaimai.palteform.baselib.base.BaseActivity;
import com.lalawaimai.palteform.baselib.http.CommonBean;
import com.lalawaimai.palteform.baselib.http.DisposeDataListener;
import com.lalawaimai.palteform.baselib.http.OkHttpException;
import com.lalawaimai.palteform.baselib.http.RequestWebInfo;
import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.CancelReasonBean;
import com.lalawaimai.plateform.constant.Constant;
import com.lalawaimai.plateform.http.WebUrl;
import com.lalawaimai.plateform.order.adapter.CancelReasonAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 取消原因选择页面
 * Created by WaterWood on 2018/6/1.
 */
public class CancelReasonActivity extends BaseActivity implements View.OnClickListener {

    private ListView listView;
    private CancelReasonAdapter mAdapter;
    private View status_bar;
    private ProgressDialog dialogWeb;//网络加载等待框
    private List<String> reasonList;
    private boolean[] listCheck;
    private LinearLayout ll_right;
    private LinearLayout ll_back;
    private TextView tv_title;
    private String reason;
    private int position;

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_cancel_reason;
    }

    @Override
    protected boolean initArgs(Bundle bundle) {
        if (bundle!=null) {
            position = bundle.getInt("position", -1);
        }
        return true;
    }

    @Override
    protected void initWidget() {
        listView = findViewById(R.id.listView);
        status_bar = findViewById(R.id.status_bar);
        ll_right = findViewById(R.id.ll_right);
        ll_back = findViewById(R.id.ll_back);
        tv_title = findViewById(R.id.tv_title);
        ll_right.setVisibility(View.VISIBLE);
        ll_right.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        tv_title.setText("取消订单原因");
        setStatusBarSize(this, status_bar);
        reasonList = new ArrayList<>();
        mAdapter = new CancelReasonAdapter(this, reasonList);
        listView.setAdapter(mAdapter);
        dialogWeb = CommonUtil.showProgressDialog(this, "正在加载");
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < listCheck.length; i++) {
                    if (i == position) {
                        reason = reasonList.get(i);
                        listCheck[i] = true;
                    } else {
                        listCheck[i] = false;
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        });
        listView.setSelector(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    protected void intiData() {
        dialogWeb.show();
        String url = WebUrl.CANCEL_REASON;
        Class<?> clazz = CancelReasonBean.class;
        List<String> list = new ArrayList<>();
        list.add("token");
        new RequestWebInfo(new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                dialogWeb.dismiss();
                CancelReasonBean cancelReasonBean = (CancelReasonBean) responseObj;
                reasonList.addAll(cancelReasonBean.getMessage().getData().getReasons());
                listCheck = new boolean[reasonList.size()];
                mAdapter.setListCheck(listCheck);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Object reasonObj) {
                dialogWeb.dismiss();
                OkHttpException okHttpException = (OkHttpException) reasonObj;
                String msg = okHttpException.getEmsg().toString();
                if (msg.contains("java.net.ConnectException")) {
                    CommonUtil.showToast(CancelReasonActivity.this, "无法连接服务器，请检查您的网络");
                }
            }
        }).requestWeb(url, clazz, list,(String) CommonUtil.readData(this, Constant.TOKEN,Constant.STRING));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_right:
                if (NullUtil.isStringEmpty(reason)) {
                    //没选了
                    CommonUtil.showToast(this, "您还未选择取消原因");
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("reason", reason);
                    intent.putExtra("position", position);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            case R.id.ll_back:
                finish();
                break;
        }
    }
}
