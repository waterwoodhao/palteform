package com.lalawaimai.palteform.baselib.picload;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.lalawaimai.palteform.baselib.R;

/**
 * 图片加载库
 * Created by WaterWood on 2018/6/5.
 */
public class ImageLoader {
    private static ImageLoader imageLoader;

    /**
     * 获取对象实例
     * @return
     */
    public static ImageLoader getInstance() {
        if (imageLoader == null) {
            imageLoader = new ImageLoader();
        }
        return imageLoader;
    }

    /**
     * 加载图片
     * @param context  上下文
     * @param imgUrl  图片地址
     * @param defaultPic  默认图片，资源文件格式
     * @param imageView 图片控件
     */
    public void loadImage(Context context, String imgUrl, int defaultPic, ImageView imageView){
        Glide.with(context).load(imgUrl).placeholder(defaultPic).dontAnimate().error(defaultPic).into(imageView);
    }
}
