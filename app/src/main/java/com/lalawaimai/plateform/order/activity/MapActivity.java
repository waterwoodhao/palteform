package com.lalawaimai.plateform.order.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdate;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.CameraPosition;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.lalawaimai.palteform.baselib.base.BaseCommonActivity;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;

/**
 * 地图页面
 * Created by WaterWood on 2018/6/5.
 */
public class MapActivity extends BaseCommonActivity implements View.OnClickListener {

    private View status_bar;
    private LinearLayout ll_back;
    private TextView tv_title;
    private MapView mMapView;
    private AMap aMap;
    private String longitude;//经度
    private String latitude;//纬度
    private CameraUpdate cameraUpdate;
    private String deliveryerName;
    private String order_takeout_num;//外卖单数
    private String order_errander_num;//跑腿单数

    @Override
    protected boolean initArgs(Bundle bundle) {
        longitude = bundle.getString("longitude");
        latitude = bundle.getString("latitude");
        deliveryerName = bundle.getString("deliveryerName");
        order_takeout_num = bundle.getString("order_takeout_num");
        order_errander_num = bundle.getString("order_errander_num");
        if (NullUtil.isStringEmpty(order_takeout_num)) {
            order_takeout_num = "0";
        }
        if (NullUtil.isStringEmpty(order_errander_num)) {
            order_errander_num = "0";
        }
        if (NullUtil.isStringEmpty(longitude) || NullUtil.isStringEmpty(latitude) || NullUtil.isStringEmpty(deliveryerName)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_map;
    }

    @Override
    protected void initWidget() {
        status_bar = findViewById(R.id.status_bar);
        ll_back = findViewById(R.id.ll_back);
        tv_title = findViewById(R.id.tv_title);
        mMapView = findViewById(R.id.map);
        ll_back.setOnClickListener(this);
        setStatusBarSize(this, status_bar);
        tv_title.setText("配送员位置");
        initMap();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_back:
                finish();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，销毁地图
        mMapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView.onResume ()，重新绘制加载地图
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView.onPause ()，暂停地图的绘制
        mMapView.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行mMapView.onSaveInstanceState (outState)，保存地图当前的状态
        mMapView.onSaveInstanceState(outState);
    }

    private void initMap() {
        mMapView.onCreate(getSavedInstanceState());
        if (aMap == null) {
            aMap = mMapView.getMap();
        }
        //开始整一个点
        LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        final Marker marker = aMap.addMarker(new MarkerOptions().position(latLng));
        marker.setTitle(deliveryerName);
        marker.setSnippet("外卖:" + order_takeout_num + "单 跑腿:" + order_errander_num + "单");
        marker.showInfoWindow();
        //可视化区域，将指定位置指定到屏幕中心位置
        cameraUpdate = CameraUpdateFactory.newCameraPosition(new CameraPosition(latLng, 18, 0, 30));
        aMap.moveCamera(cameraUpdate);
    }
}
