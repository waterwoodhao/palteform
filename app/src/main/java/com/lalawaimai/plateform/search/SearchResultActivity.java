package com.lalawaimai.plateform.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.lalawaimai.palteform.baselib.base.BaseActivity;
import com.lalawaimai.palteform.baselib.http.DisposeDataListener;
import com.lalawaimai.palteform.baselib.http.OkHttpException;
import com.lalawaimai.palteform.baselib.http.RequestWebInfo;
import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.OrderListBean;
import com.lalawaimai.plateform.bean.OrderListEntity;
import com.lalawaimai.plateform.constant.Constant;
import com.lalawaimai.plateform.http.WebUrl;
import com.lalawaimai.plateform.order.activity.OrderDetailActivity;
import com.lalawaimai.plateform.order.adapter.MoreOrderAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import java.util.ArrayList;
import java.util.List;

/**
 * 搜索结果页面
 * Created by WaterWood on 2018/6/2.
 */
public class SearchResultActivity extends BaseActivity implements View.OnClickListener{

    private String uid;
    private String keyword;
    private String deliveryer_id;
    private String sid;
    private String agentid;
    private String addtime_start;
    private String addtime_end;
    private View status_bar;
    private TextView tv_title;
    private LinearLayout ll_back;
    private SmartRefreshLayout refreshLayout;
    private int page = 1;
    private List<OrderListEntity> list_order;//列表中的内容
    private final int MAX_PAGE = 20;//每页最大数量
    private MoreOrderAdapter moreOrderAdapter;
    private ListView lv_order;
    private RelativeLayout rl_empty;
    private TextView tv_empty;

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_search_result;
    }

    @Override
    protected boolean initArgs(Bundle bundle) {
        uid = bundle.getString("uid");
        keyword = bundle.getString("keyword");
        deliveryer_id = bundle.getString("deliveryer_id");
        sid = bundle.getString("sid");
        agentid = bundle.getString("agentid");
        addtime_start = bundle.getString("addtime_start");
        addtime_end = bundle.getString("addtime_end");
        return true;
    }

    @Override
    protected void initWidget() {
        status_bar = findViewById(R.id.status_bar);
        tv_title = findViewById(R.id.tv_title);
        ll_back = findViewById(R.id.ll_back);
        refreshLayout = findViewById(R.id.refreshLayout);
        lv_order = findViewById(R.id.lv_order);
        rl_empty = findViewById(R.id.rl_empty);
        tv_empty = findViewById(R.id.tv_empty);
        ll_back.setOnClickListener(this);
        setStatusBarSize(this,status_bar);
        tv_title.setText("订单搜索结果");
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 1;
                intiData();
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                page++;
                intiData();
            }
        });
        list_order = new ArrayList<>();
        moreOrderAdapter = new MoreOrderAdapter(this, list_order);
        lv_order.setAdapter(moreOrderAdapter);
        tv_empty.setText("没有搜索到相关订单");
        lv_order.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SearchResultActivity.this, OrderDetailActivity.class);
                intent.putExtra("id", list_order.get(position).getId());
                intent.putExtra("status", list_order.get(position).getStatus());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void intiData() {
        String url = WebUrl.GET_ORDER_LIST;
        Class<?> clazz = OrderListBean.class;
        List<String> list = new ArrayList<>();
        list.add("token");
        list.add("page");
        list.add("uid");
        list.add("keyword");
        list.add("sid");
        list.add("agentid");
        list.add("deliveryer_id");
        list.add("addtime_start");
        list.add("addtime_end");
        new RequestWebInfo(new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                OrderListBean orderListBean = (OrderListBean) responseObj;
                if (page == 1) {
                    refreshLayout.finishRefresh();
                    //刷新操作
                    List<OrderListEntity> list = orderListBean.getMessage().getData().getOrder();
                    list_order.clear();
                    if (!NullUtil.isListEmpty(list)) {
                        //有东西
                        list_order.addAll(list);
                        //如果第一页就小于二十条，直接禁止加载更多
                        if (list.size() < MAX_PAGE) {
                            refreshLayout.setEnableLoadMore(false);
                        } else {
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                        rl_empty.setVisibility(View.GONE);
                    } else {
                        //没有东西，显示空页面，隐藏列表
                        rl_empty.setVisibility(View.VISIBLE);
                    }
                    moreOrderAdapter.notifyDataSetChanged();
                } else {
                    //加载更多
                    refreshLayout.finishLoadMore();
                    List<OrderListEntity> list = orderListBean.getMessage().getData().getOrder();
                    if (!NullUtil.isListEmpty(list)) {
                        //有东西
                        list_order.addAll(list);
                        //如果这一页小于这么多，直接禁止加载更多
                        if (list.size() < MAX_PAGE) {
                            refreshLayout.setEnableLoadMore(false);
                        } else {
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                        moreOrderAdapter.notifyDataSetChanged();
                        rl_empty.setVisibility(View.GONE);
                    } else {
                        //没东西
                        CommonUtil.showToast(SearchResultActivity.this, "已经加载到底了");
                        refreshLayout.setEnableLoadMore(false);
                    }
                }
            }

            @Override
            public void onFailure(Object reasonObj) {
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
                OkHttpException okHttpException = (OkHttpException) reasonObj;
                String msg = okHttpException.getEmsg().toString();
                if (msg.contains("java.net.ConnectException")) {
                    CommonUtil.showToast(SearchResultActivity.this, "无法连接服务器，请检查您的网络");
                }
            }
        }).requestWeb(url, clazz, list, (String) CommonUtil.readData(this, Constant.TOKEN,Constant.STRING),page+"",uid,keyword,sid,agentid,deliveryer_id,addtime_start,addtime_end);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_back:
                finish();
                break;
        }
    }
}
