package com.lalawaimai.plateform.application;

import android.app.Application;
import android.content.Context;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.lalawaimai.palteform.baselib.config.Configration;
import cn.jpush.android.api.JPushInterface;

/**
 * 自定义Application
 * Created by WaterWood on 2018/6/1.
 */
public class MyApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        this.context = this;
        Configration.getInstance().initIconify();
        //极光推送初始化
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
        //科大讯飞初始化，appid在这里填写
        SpeechUtility.createUtility(this, SpeechConstant.APPID +"=5b1a341a");
    }

    public static Context getContext(){
        return context;
    }
}
