package com.lalawaimai.palteform.baselib.http;

import java.io.FileNotFoundException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 封装request参数存储类
 * Created by WaterWood on 2018/5/29.
 */
public class RequestParams {
    //普通参数保存集合
    public ConcurrentHashMap<String, String> urlParams = new ConcurrentHashMap<>();
    //文件参数保存集合
    public ConcurrentHashMap<String, Object> fileParams = new ConcurrentHashMap<>();

    /**
     * 普通参数放置方法
     *
     * @param key
     * @param value
     */
    public void put(String key, String value) {
        if (key != null && value != null) {
            urlParams.put(key, value);
        }
    }

    /**
     * 文件参数放置方法
     * @param key
     * @param object
     * @throws FileNotFoundException
     */
    public void put(String key, Object object) throws FileNotFoundException {
        if (key != null) {
            fileParams.put(key, object);
        }
    }
}
