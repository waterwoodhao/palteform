package com.lalawaimai.plateform.order.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lalawaimai.palteform.baselib.base.BaseFragment;
import com.lalawaimai.palteform.baselib.http.DisposeDataListener;
import com.lalawaimai.palteform.baselib.http.OkHttpException;
import com.lalawaimai.palteform.baselib.http.RequestWebInfo;
import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.OrderListBean;
import com.lalawaimai.plateform.bean.OrderListEntity;
import com.lalawaimai.plateform.constant.Constant;
import com.lalawaimai.plateform.http.WebUrl;
import com.lalawaimai.plateform.order.activity.DispatchMapActivity;
import com.lalawaimai.plateform.order.activity.OrderDetailActivity;
import com.lalawaimai.plateform.order.adapter.MoreOrderAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import java.util.ArrayList;
import java.util.List;

/**
 * 更多订单内页
 * Created by WaterWood on 2018/6/1.
 */
public class MoreOrderFragment extends BaseFragment {

    private int flag;//各个内页区分
    private ListView lv_order;
    private MoreOrderAdapter moreOrderAdapter;
    private SmartRefreshLayout refreshLayout;
    private int page = 1;
    private String status;
    private List<OrderListEntity> list_order;//列表中的内容
    private final int MAX_PAGE = 20;//每页最大数量
    private RelativeLayout rl_empty;
    private TextView tv_empty;

    @Override
    protected int getContentLayoutId() {
        return R.layout.fragment_more_order;
    }

    @Override
    protected void initArgs(Bundle bundle) {
        flag = bundle.getInt("flag", 0);
        if (flag == 1) {
            status = "5";
        } else if (flag == 2) {
            status = "6";
        } else if (flag == 3) {
            status = "8";
        }
    }

    @Override
    protected void initWidget(View root) {
        //初始化控件
        lv_order = root.findViewById(R.id.lv_order);
        refreshLayout = root.findViewById(R.id.refreshLayout);
        rl_empty = root.findViewById(R.id.rl_empty);
        tv_empty = root.findViewById(R.id.tv_empty);
        //初始化布局
        list_order = new ArrayList<>();
        moreOrderAdapter = new MoreOrderAdapter(getActivity(), list_order);
        lv_order.setAdapter(moreOrderAdapter);
        lv_order.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
                intent.putExtra("id", list_order.get(position).getId());
                intent.putExtra("status", status);
                intent.putExtra("isMore",true);
                startActivity(intent);
            }
        });
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 1;
                getOrderList();
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                page++;
                getOrderList();
            }
        });
        tv_empty.setText("没有相关订单");
    }

    @Override
    protected void initData() {
        refreshLayout.autoRefresh();
    }

    private void getOrderList() {
        String url = WebUrl.GET_ORDER_LIST;
        Class<?> clazz = OrderListBean.class;
        List<String> list = new ArrayList<>();
        list.add("status");
        list.add("page");
        list.add("token");
        new RequestWebInfo(new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                String json = RequestWebInfo.jsonInfo;
                OrderListBean orderListBean = (OrderListBean) responseObj;
                if (page == 1) {
                    refreshLayout.finishRefresh();
                    //刷新操作
                    List<OrderListEntity> list = orderListBean.getMessage().getData().getOrder();
                    list_order.clear();
                    if (!NullUtil.isListEmpty(list)) {
                        //有东西
                        list_order.addAll(list);
                        //如果第一页就小于二十条，直接禁止加载更多
                        if (list.size() < MAX_PAGE) {
                            refreshLayout.setEnableLoadMore(false);
                        } else {
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                        rl_empty.setVisibility(View.GONE);
                    } else {
                        //没有东西，显示空页面，隐藏列表
                        rl_empty.setVisibility(View.VISIBLE);
                    }
                    moreOrderAdapter.notifyDataSetChanged();
                } else {
                    //加载更多
                    refreshLayout.finishLoadMore();
                    List<OrderListEntity> list = orderListBean.getMessage().getData().getOrder();
                    if (!NullUtil.isListEmpty(list)) {
                        //有东西
                        list_order.addAll(list);
                        //如果这一页小于这么多，直接禁止加载更多
                        if (list.size() < MAX_PAGE) {
                            refreshLayout.setEnableLoadMore(false);
                        } else {
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                        moreOrderAdapter.notifyDataSetChanged();
                        rl_empty.setVisibility(View.GONE);
                    } else {
                        //没东西
                        CommonUtil.showToast(getActivity(), "已经加载到底了");
                        refreshLayout.setEnableLoadMore(false);
                    }
                }
            }

            @Override
            public void onFailure(Object reasonObj) {
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
                OkHttpException okHttpException = (OkHttpException) reasonObj;
                String msg = okHttpException.getEmsg().toString();
                if (msg.contains("java.net.ConnectException")) {
                    CommonUtil.showToast(getActivity(), "无法连接服务器，请检查您的网络");
                }else{
                    CommonUtil.showToast(getActivity(), msg);
                }
            }
        }).requestWeb(url, clazz, list, status, page + "",(String) CommonUtil.readData(getActivity(), Constant.TOKEN,Constant.STRING));
    }
}
