package com.lalawaimai.plateform.order.inter;

/**
 * 按钮生效回调接口
 * Created by WaterWood on 2018/6/1.
 */
public interface IButtonRequest {
    public void buttonClick(String type,int position,String reason);//按钮点击后的相关请求
    public void cancleJump(int position);//取消跳转
    public void diaodu(int position);
}
