package com.lalawaimai.plateform.bean;

import java.util.List;

/**
 * 订单列表实体类
 * Created by WaterWood on 2018/5/31.
 */
public class OrderListBean {

    private MessageBean message;
    private String redirect;
    private String type;

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class MessageBean {

        private int resultCode;
        private String resultMessage;
        private DataBean data;

        public int getResultCode() {
            return resultCode;
        }

        public void setResultCode(int resultCode) {
            this.resultCode = resultCode;
        }

        public String getResultMessage() {
            return resultMessage;
        }

        public void setResultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            private List<OrderListEntity> order;

            public List<OrderListEntity> getOrder() {
                return order;
            }

            public void setOrder(List<OrderListEntity> order) {
                this.order = order;
            }
        }
    }
}
