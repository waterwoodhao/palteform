package com.lalawaimai.plateform.bean;

import java.util.List;

/**
 * 极光推送附加信息实体
 * Created by WaterWood on 2018/6/8.
 */
public class JpushMsgBean {

    private AudienceBean audience;
    private String redirect_extra;
    private String redirect_type;
    private int voice_play_nums;
    private String voice_text;
    private String order_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public AudienceBean getAudience() {
        return audience;
    }

    public void setAudience(AudienceBean audience) {
        this.audience = audience;
    }

    public String getRedirect_extra() {
        return redirect_extra;
    }

    public void setRedirect_extra(String redirect_extra) {
        this.redirect_extra = redirect_extra;
    }

    public String getRedirect_type() {
        return redirect_type;
    }

    public void setRedirect_type(String redirect_type) {
        this.redirect_type = redirect_type;
    }

    public int getVoice_play_nums() {
        return voice_play_nums;
    }

    public void setVoice_play_nums(int voice_play_nums) {
        this.voice_play_nums = voice_play_nums;
    }

    public String getVoice_text() {
        return voice_text;
    }

    public void setVoice_text(String voice_text) {
        this.voice_text = voice_text;
    }

    public static class AudienceBean {
        private List<String> tag;

        public List<String> getTag() {
            return tag;
        }

        public void setTag(List<String> tag) {
            this.tag = tag;
        }
    }
}
