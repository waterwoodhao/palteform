package com.lalawaimai.plateform.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.widget.RadioGroup;
import com.lalawaimai.palteform.baselib.base.BaseActivity;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.palteform.baselib.widge.NoScrollViewPager;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.order.activity.OrderDetailActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页
 * Created by WaterWood on 2018/5/30.
 */
public class HomeActivity extends BaseActivity{

    private RadioGroup rg_group;
    private NoScrollViewPager viewPager;
    private List<Fragment> list_fragment;
    private FragmentPagerAdapter mAdapter;
    private String orderId;

    @Override
    protected boolean initArgs(Bundle bundle) {
        Intent intent = getIntent();
        boolean flag = intent.getBooleanExtra("flag",false);
        if (flag) {
            orderId = intent.getStringExtra("orderId");
        }
        return true;
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void initWidget() {
        //初始化控件
        rg_group = findViewById(R.id.rg_group);
        viewPager = findViewById(R.id.viewPager);
        //其他初始化操作
        rg_group.check(R.id.rb_order);
        rg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_order:
                        viewPager.setCurrentItem(0,false);
                        break;
                    case R.id.rb_shop:
                        viewPager.setCurrentItem(1,false);
                        break;
                    case R.id.rb_more:
                        viewPager.setCurrentItem(2,false);
                        break;
                    case R.id.rb_mine:
                        viewPager.setCurrentItem(3,false);
                        break;
                }
            }
        });
        list_fragment = new ArrayList<>();
        list_fragment.add(new OrderFragment());
        list_fragment.add(new ShopFragment());
        list_fragment.add(new MoreFragment());
        list_fragment.add(new MineFragment());
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return list_fragment.get(position);
            }

            @Override
            public int getCount() {
                return list_fragment.size();
            }
        };
        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        initJump();
    }

    @Override
    protected void intiData() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            return true;
        }else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void initJump(){
        if (!NullUtil.isStringEmpty(orderId)){
            Intent intent = new Intent(this, OrderDetailActivity.class);
            intent.putExtra("id",orderId);
            startActivity(intent);
        }
    }
}
