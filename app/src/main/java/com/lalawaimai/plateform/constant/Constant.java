package com.lalawaimai.plateform.constant;

/**
 * 常量类集合
 * Created by WaterWood on 2018/6/8.
 */
public class Constant {
    public static final String UNIACID = "uniacid";//序列号id
    public static final String URL = "url";//网址
    public static final String TOKEN = "token";//用户识别码
    public static final String INT = "int";//整形标记
    public static final String STRING = "string";//字符串标记
    public static final String BOOLEAN = "boolean";//布尔值标记
}
