package com.lalawaimai.plateform.shop.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.lalawaimai.palteform.baselib.base.BaseMineAdapter;
import com.lalawaimai.palteform.baselib.http.CommonBean;
import com.lalawaimai.palteform.baselib.picload.ImageLoader;
import com.lalawaimai.palteform.baselib.widge.roundimageview.RoundedImageView;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.ShopListBean;
import com.lalawaimai.plateform.bean.ShopListEntity;

import java.util.List;

/**
 * 店铺列表适配器
 * Created by WaterWood on 2018/6/4.
 */
public class ShopListAdapter extends BaseMineAdapter<ShopListEntity>{
    /**
     * 如果这个不止一个列表，就在子类的Adapter中重写一个set方法放进来
     *
     * @param activity
     * @param list
     */
    public ShopListAdapter(Activity activity, List<ShopListEntity> list) {
        super(activity, list);
    }

    @Override
    protected Object getHolderChild() {
        return new MineHolder();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_shop_list;
    }

    @Override
    protected void initHolderWidge(View convertView, Object object) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.photo = convertView.findViewById(R.id.photo);
        mineHolder.shop_name = convertView.findViewById(R.id.shop_name);
        mineHolder.tv_isClose = convertView.findViewById(R.id.tv_isClose);
        mineHolder.tv_phone = convertView.findViewById(R.id.tv_phone);
        mineHolder.tv_address = convertView.findViewById(R.id.tv_address);
    }

    @Override
    protected void initHolderData(Object object, int position) {
        MineHolder mineHolder = (MineHolder) object;
        ImageLoader.getInstance().loadImage(activity,list.get(position).getLogo(),R.mipmap.ic_load_fail,mineHolder.photo);
        mineHolder.shop_name.setText(list.get(position).getTitle());
        mineHolder.tv_isClose.setText(list.get(position).getStatus_cn());
        mineHolder.tv_phone.setText("电话："+list.get(position).getTelephone());
        mineHolder.tv_address.setText("地址："+list.get(position).getAddress());
    }

    private class MineHolder{
        public RoundedImageView photo;
        public TextView shop_name;
        public TextView tv_isClose;
        public TextView tv_phone;
        public TextView tv_address;
    }
}
