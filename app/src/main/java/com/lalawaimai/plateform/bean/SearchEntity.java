package com.lalawaimai.plateform.bean;

/**
 * 搜索中配送员，店铺，代理实体
 * Created by WaterWood on 2018/6/2.
 */
public class SearchEntity {
    private String id;
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
