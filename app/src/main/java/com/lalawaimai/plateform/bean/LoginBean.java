package com.lalawaimai.plateform.bean;

import java.util.List;

/**
 * 登录实体
 * Created by WaterWood on 2018/6/8.
 */
public class LoginBean {

    private MessageBean message;
    private String redirect;
    private String type;

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class MessageBean {

        private int resultCode;
        private String resultMessage;
        private DataBean data;

        public int getResultCode() {
            return resultCode;
        }

        public void setResultCode(int resultCode) {
            this.resultCode = resultCode;
        }

        public String getResultMessage() {
            return resultMessage;
        }

        public void setResultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {

            private String uid;
            private String groupid;
            private String username;
            private String password;
            private String salt;
            private String status;
            private String joindate;
            private String joinip;
            private String lastvisit;
            private String lastip;
            private String remark;
            private String starttime;
            private String endtime;
            private String token;
            private JpushRelationBean jpush_relation;

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getGroupid() {
                return groupid;
            }

            public void setGroupid(String groupid) {
                this.groupid = groupid;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getSalt() {
                return salt;
            }

            public void setSalt(String salt) {
                this.salt = salt;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getJoindate() {
                return joindate;
            }

            public void setJoindate(String joindate) {
                this.joindate = joindate;
            }

            public String getJoinip() {
                return joinip;
            }

            public void setJoinip(String joinip) {
                this.joinip = joinip;
            }

            public String getLastvisit() {
                return lastvisit;
            }

            public void setLastvisit(String lastvisit) {
                this.lastvisit = lastvisit;
            }

            public String getLastip() {
                return lastip;
            }

            public void setLastip(String lastip) {
                this.lastip = lastip;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getStarttime() {
                return starttime;
            }

            public void setStarttime(String starttime) {
                this.starttime = starttime;
            }

            public String getEndtime() {
                return endtime;
            }

            public void setEndtime(String endtime) {
                this.endtime = endtime;
            }

            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }

            public JpushRelationBean getJpush_relation() {
                return jpush_relation;
            }

            public void setJpush_relation(JpushRelationBean jpush_relation) {
                this.jpush_relation = jpush_relation;
            }

            public static class JpushRelationBean {

                private String alias;
                private List<String> tags;

                public String getAlias() {
                    return alias;
                }

                public void setAlias(String alias) {
                    this.alias = alias;
                }

                public List<String> getTags() {
                    return tags;
                }

                public void setTags(List<String> tags) {
                    this.tags = tags;
                }
            }
        }
    }
}
