package com.lalawaimai.plateform.order.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lalawaimai.palteform.baselib.base.BaseMineAdapter;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.OrderListEntity;
import com.lalawaimai.plateform.order.activity.CancelReasonActivity;
import com.lalawaimai.plateform.order.fragment.WaitOrderFragment;
import com.lalawaimai.plateform.order.inter.IButtonRequest;

import java.util.List;

/**
 * 订单列表适配器
 * Created by WaterWood on 2018/5/30.
 */
public class WaitOrderAdapter extends BaseMineAdapter<OrderListEntity> {

    private String type;
    private IButtonRequest iButtonRequest;

    public WaitOrderAdapter(Activity activity, List<OrderListEntity> list) {
        super(activity, list);
    }

    /**
     * 获取Holder的具体实现
     *
     * @return
     */
    @Override
    protected Object getHolderChild() {
        return new MineHolder();
    }

    /**
     * 获取item布局
     *
     * @return
     */
    @Override
    protected int getLayoutId() {
        return R.layout.item_wait_order;
    }

    /**
     * 初始化Holder中的控件
     *
     * @param convertView
     * @param object
     */
    @Override
    protected void initHolderWidge(View convertView, Object object) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.bottom_line = convertView.findViewById(R.id.bottom_line);
        mineHolder.number = convertView.findViewById(R.id.number);
        mineHolder.price = convertView.findViewById(R.id.price);
        mineHolder.order_get_shop = convertView.findViewById(R.id.order_get_shop);
        mineHolder.order_get_address = convertView.findViewById(R.id.order_get_address);
        mineHolder.order_send_address = convertView.findViewById(R.id.order_send_address);
        mineHolder.name_and_phone = convertView.findViewById(R.id.name_and_phone);
        mineHolder.note = convertView.findViewById(R.id.note);
        mineHolder.add_time = convertView.findViewById(R.id.add_time);
        mineHolder.delivery_time = convertView.findViewById(R.id.delivery_time);
        mineHolder.bt1 = convertView.findViewById(R.id.bt1);
        mineHolder.bt2 = convertView.findViewById(R.id.bt2);
        mineHolder.bt3 = convertView.findViewById(R.id.bt3);
        mineHolder.bt4 = convertView.findViewById(R.id.bt4);
        mineHolder.tv_deliveryer = convertView.findViewById(R.id.tv_deliveryer);
        mineHolder.ll_deliveryer = convertView.findViewById(R.id.ll_deliveryer);
    }

    /**
     * 初始化Holder中的控件数据
     *
     * @param object
     */
    @Override
    protected void initHolderData(Object object, final int position) {
        MineHolder mineHolder = (MineHolder) object;
        if (position == list.size() - 1) {
            mineHolder.bottom_line.setVisibility(View.GONE);
        } else {
            mineHolder.bottom_line.setVisibility(View.VISIBLE);
        }
        mineHolder.number.setText("#" + list.get(position).getSerial_sn());
        mineHolder.price.setText("￥" + list.get(position).getFinal_fee());
        if (list.get(position).getStore()!=null) {
            mineHolder.order_get_shop.setText(list.get(position).getStore().getTitle());
            mineHolder.order_get_address.setText(list.get(position).getStore().getAddress());
        }else{
            mineHolder.order_get_shop.setText("");
            mineHolder.order_get_address.setText("");
        }
        mineHolder.order_send_address.setText(list.get(position).getAddress());
        mineHolder.name_and_phone.setText(list.get(position).getUsername() + " " + list.get(position).getMobile());
        mineHolder.note.setText(list.get(position).getNote());
        mineHolder.add_time.setText(list.get(position).getAddtime_cn());
        mineHolder.delivery_time.setText(list.get(position).getDeliverytime_cn());
        if (type.equals("2") || type.equals("3")) {
            mineHolder.bt1.setText("取消");
            mineHolder.bt2.setText("通知配送员");
            mineHolder.bt3.setText("调度");
        } else if (type.equals("4")) {
            mineHolder.bt4.setVisibility(View.VISIBLE);
            mineHolder.bt1.setText("重置为待抢");
            mineHolder.bt2.setText("调度");
            mineHolder.bt3.setText("完成");
            mineHolder.bt4.setText("取消");
        } else if (type.equals("7")) {
            mineHolder.bt1.setText("已退款");
            mineHolder.bt2.setText("查询退款进度");
            mineHolder.bt3.setText("发起退款");
        }
        if (type.equals("1")) {
            mineHolder.bt3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //确认接单
                    iButtonRequest.buttonClick("handle", position, "");
                }
            });
            mineHolder.bt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //通知商户接单
                    iButtonRequest.buttonClick("notify_clerk_handle", position, "");
                }
            });
            mineHolder.bt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //取消待抢订单
                    iButtonRequest.cancleJump(position);
                }
            });
        } else if (type.equals("2")) {
            mineHolder.bt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.cancleJump(position);
                }
            });
            mineHolder.bt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //通知配送员抢单
                    iButtonRequest.buttonClick("notify_deliveryer_collect", position, "");
                }
            });
            mineHolder.bt3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.diaodu(position);
                }
            });
        } else if (type.equals("3")) {
            mineHolder.bt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.cancleJump(position);
                }
            });
            mineHolder.bt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //通知配送员抢单
                    iButtonRequest.buttonClick("notify_deliveryer_collect", position, "");
                }
            });
            mineHolder.bt3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.diaodu(position);
                }
            });
        } else if (type.equals("4")) {
            mineHolder.bt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.buttonClick("re_notify_deliveryer_collect", position, "");
                }
            });
            mineHolder.bt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.diaodu(position);
                }
            });
            mineHolder.bt3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.buttonClick("end", position, "");
                }
            });
            mineHolder.bt4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.cancleJump(position);
                }
            });
        } else if (type.equals("7")) {
            mineHolder.bt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.buttonClick("refund_status", position, "");
                }
            });
            mineHolder.bt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.buttonClick("refund_query", position, "");
                }
            });
            mineHolder.bt3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iButtonRequest.buttonClick("refund_handle", position, "");
                }
            });
        }
        if (list.get(position).getDeliveryer_id().equals("0")) {
            mineHolder.ll_deliveryer.setVisibility(View.GONE);
        } else {
            mineHolder.ll_deliveryer.setVisibility(View.VISIBLE);
            mineHolder.tv_deliveryer.setText(list.get(position).getDeliveryerBean().getTitle());
        }
    }

    public class MineHolder {
        public View bottom_line;
        public TextView number;
        public TextView price;
        public TextView order_get_shop;
        public TextView order_get_address;
        public TextView order_send_address;
        public TextView name_and_phone;
        public TextView note;
        public TextView add_time;
        public TextView delivery_time;
        public TextView bt1;
        public TextView bt2;
        public TextView bt3;
        public TextView bt4;
        public TextView tv_deliveryer;
        public LinearLayout ll_deliveryer;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setiButtonRequest(IButtonRequest iButtonRequest) {
        this.iButtonRequest = iButtonRequest;
    }
}
