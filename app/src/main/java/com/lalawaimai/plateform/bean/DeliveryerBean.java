package com.lalawaimai.plateform.bean;

import java.io.Serializable;

/**
 * 调度页面需要传入的各种数据
 * Created by WaterWood on 2018/6/5.
 */
public class DeliveryerBean implements Serializable {
    private String id;
    private String name;
    private Double longitude;
    private Double latitude;
    private String order_takeout_num;
    private String order_errander_num;
    private Double longitudeShop;
    private Double latitudeShop;
    private Double longitudeCustomer;
    private Double latitudeCustomer;

    public Double getLongitudeShop() {
        return longitudeShop;
    }

    public void setLongitudeShop(Double longitudeShop) {
        this.longitudeShop = longitudeShop;
    }

    public Double getLatitudeShop() {
        return latitudeShop;
    }

    public void setLatitudeShop(Double latitudeShop) {
        this.latitudeShop = latitudeShop;
    }

    public Double getLongitudeCustomer() {
        return longitudeCustomer;
    }

    public void setLongitudeCustomer(Double longitudeCustomer) {
        this.longitudeCustomer = longitudeCustomer;
    }

    public Double getLatitudeCustomer() {
        return latitudeCustomer;
    }

    public void setLatitudeCustomer(Double latitudeCustomer) {
        this.latitudeCustomer = latitudeCustomer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getOrder_takeout_num() {
        return order_takeout_num;
    }

    public void setOrder_takeout_num(String order_takeout_num) {
        this.order_takeout_num = order_takeout_num;
    }

    public String getOrder_errander_num() {
        return order_errander_num;
    }

    public void setOrder_errander_num(String order_errander_num) {
        this.order_errander_num = order_errander_num;
    }
}
