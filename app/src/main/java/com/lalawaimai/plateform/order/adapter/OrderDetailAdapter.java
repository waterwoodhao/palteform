package com.lalawaimai.plateform.order.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.lalawaimai.palteform.baselib.base.BaseMineAdapter;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.OrderGoodsBean;
import java.util.List;

public class OrderDetailAdapter extends BaseMineAdapter<OrderGoodsBean>{

    /**
     * 如果这个不止一个列表，就在子类的Adapter中重写一个set方法放进来
     *
     * @param activity
     * @param list
     */
    public OrderDetailAdapter(Activity activity, List<OrderGoodsBean> list) {
        super(activity, list);
    }

    @Override
    protected Object getHolderChild() {
        return new MineHolder();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_order_detail;
    }

    @Override
    protected void initHolderWidge(View convertView, Object object) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.goods_name = convertView.findViewById(R.id.goods_name);
        mineHolder.goods_num = convertView.findViewById(R.id.goods_num);
        mineHolder.goods_price = convertView.findViewById(R.id.goods_price);
    }

    @Override
    protected void initHolderData(Object object, int position) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.goods_name.setText(list.get(position).getGoods_title());
        mineHolder.goods_num.setText(list.get(position).getGoods_num());
        mineHolder.goods_price.setText("￥"+list.get(position).getGoods_price());
    }

    private class MineHolder{
        public TextView goods_name;
        public TextView goods_num;
        public TextView goods_price;
    }
}
