package com.lalawaimai.plateform.home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.lalawaimai.palteform.baselib.base.BaseFragment;
import com.lalawaimai.palteform.baselib.http.DisposeDataListener;
import com.lalawaimai.palteform.baselib.http.OkHttpException;
import com.lalawaimai.palteform.baselib.http.RequestWebInfo;
import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.ShopListBean;
import com.lalawaimai.plateform.bean.ShopListEntity;
import com.lalawaimai.plateform.constant.Constant;
import com.lalawaimai.plateform.http.WebUrl;
import com.lalawaimai.plateform.order.activity.DispatchMapActivity;
import com.lalawaimai.plateform.shop.activity.SearchShopListActivity;
import com.lalawaimai.plateform.shop.adapter.ShopListAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import java.util.ArrayList;
import java.util.List;

/**
 * 店铺页面
 * Created by WaterWood on 2018/6/4.
 */
public class ShopFragment extends BaseFragment implements View.OnClickListener{

    private TextView tv_title;
    private HomeActivity homeActivity;
    private View status_bar;
    private ShopListAdapter mAdapter;
    private List<ShopListEntity> listShop;
    private ListView lv_order;
    private int page;
    private SmartRefreshLayout refreshLayout;
    private final int MAX_PAGE = 20;
    private RelativeLayout rl_empty;
    private LinearLayout ll_search;
    private TextView tv_empty;

    @Override
    protected int getContentLayoutId() {
        return R.layout.fragment_shop;
    }

    @Override
    protected void initWidget(View root) {
        tv_title = root.findViewById(R.id.tv_title);
        status_bar = root.findViewById(R.id.status_bar);
        lv_order = root.findViewById(R.id.lv_order);
        refreshLayout = root.findViewById(R.id.refreshLayout);
        rl_empty = root.findViewById(R.id.rl_empty);
        ll_search = root.findViewById(R.id.ll_search);
        tv_empty = root.findViewById(R.id.tv_empty);
        ll_search.setOnClickListener(this);
        tv_title.setText("店铺");
        tv_empty.setText("当前没有店铺");
        homeActivity = (HomeActivity) getActivity();
        homeActivity.setStatusBarSize(context,status_bar);
        listShop = new ArrayList<>();
        mAdapter = new ShopListAdapter(getActivity(),listShop);
        lv_order.setAdapter(mAdapter);
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 1;
                getOrderList();
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                page++;
                getOrderList();
            }
        });
        lv_order.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO: 2018/6/4 项点击出发
            }
        });
    }

    @Override
    protected void initData() {
        refreshLayout.autoRefresh();
    }

    /**
     * 获取数据
     */
    private void getOrderList() {
        String url = WebUrl.GET_SHOP_LIST;
        Class<?> clazz = ShopListBean.class;
        List<String> list = new ArrayList<>();
        list.add("page");
        list.add("token");
        new RequestWebInfo(new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                ShopListBean shopListBean = (ShopListBean) responseObj;
                if (page == 1) {
                    refreshLayout.finishRefresh();
                    //刷新操作
                    List<ShopListEntity> list = shopListBean.getMessage().getData().getStores();
                    listShop.clear();
                    if (!NullUtil.isListEmpty(list)) {
                        //有东西
                        listShop.addAll(list);
                        //如果第一页就小于二十条，直接禁止加载更多
                        if (list.size() < MAX_PAGE) {
                            refreshLayout.setEnableLoadMore(false);
                        } else {
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                        rl_empty.setVisibility(View.GONE);
                    } else {
                        //没有东西，显示空页面，隐藏列表
                        rl_empty.setVisibility(View.VISIBLE);
                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    //加载更多
                    refreshLayout.finishLoadMore();
                    List<ShopListEntity> list = shopListBean.getMessage().getData().getStores();
                    if (!NullUtil.isListEmpty(list)) {
                        //有东西
                        listShop.addAll(list);
                        //如果这一页小于这么多，直接禁止加载更多
                        if (list.size() < MAX_PAGE) {
                            refreshLayout.setEnableLoadMore(false);
                        } else {
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                        rl_empty.setVisibility(View.GONE);
                    } else {
                        //没东西
                        CommonUtil.showToast(getActivity(), "已经加载到底了");
                        refreshLayout.setEnableLoadMore(false);
                    }
                }
            }

            @Override
            public void onFailure(Object reasonObj) {
                page--;
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
                OkHttpException okHttpException = (OkHttpException) reasonObj;
                String msg = okHttpException.getEmsg().toString();
                if (msg.contains("java.net.ConnectException")) {
                    CommonUtil.showToast(getActivity(), "无法连接服务器，请检查您的网络");
                }else{
                    CommonUtil.showToast(getActivity(), msg);
                }
            }
        }).requestWeb(url, clazz, list, page + "",(String) CommonUtil.readData(getActivity(), Constant.TOKEN,Constant.STRING));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_search:
                Intent intent = new Intent(context, SearchShopListActivity.class);
                startActivity(intent);
                break;
        }
    }
}
