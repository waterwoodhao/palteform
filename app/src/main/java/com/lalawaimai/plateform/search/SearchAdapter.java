package com.lalawaimai.plateform.search;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.lalawaimai.palteform.baselib.base.BaseMineAdapter;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.SearchEntity;
import java.util.List;

public class SearchAdapter extends BaseMineAdapter<SearchEntity>{

    private List<Boolean> deliveryersListCheck;

    /**
     * 如果这个不止一个列表，就在子类的Adapter中重写一个set方法放进来
     *
     * @param activity
     * @param list
     */
    public SearchAdapter(Activity activity, List<SearchEntity> list) {
        super(activity, list);
    }

    @Override
    protected Object getHolderChild() {
        return new MineHolder();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_cancel_reason;
    }

    @Override
    protected void initHolderWidge(View convertView, Object object) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.reason_name = convertView.findViewById(R.id.reason_name);
        mineHolder.iv_check = convertView.findViewById(R.id.iv_check);
        mineHolder.line = convertView.findViewById(R.id.line);
    }

    @Override
    protected void initHolderData(Object object, int position) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.reason_name.setText(list.get(position).getTitle());
        if (position == list.size()-1){
            mineHolder.line.setVisibility(View.GONE);
        }else{
            mineHolder.line.setVisibility(View.VISIBLE);
        }
        if (deliveryersListCheck.get(position)){
            mineHolder.iv_check.setVisibility(View.VISIBLE);
        }else{
            mineHolder.iv_check.setVisibility(View.GONE);
        }
    }

    private class MineHolder{
        public TextView reason_name;
        public ImageView iv_check;
        public View line;
    }

    /**
     * 设置选中判断序列
     * @param deliveryersListCheck
     */
    public void setDeliveryersListCheck(List<Boolean> deliveryersListCheck) {
        this.deliveryersListCheck = deliveryersListCheck;
    }
}
