package com.lalawaimai.plateform.bean;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * 请输入该类注释
 * Created by WaterWood on 2018/6/5.
 */
public class DispatchBean {

    private MessageBean message;
    private String redirect;
    private String type;

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class MessageBean {

        private int resultCode;
        private String resultMessage;
        private DataBeanX data;

        public int getResultCode() {
            return resultCode;
        }

        public void setResultCode(int resultCode) {
            this.resultCode = resultCode;
        }

        public String getResultMessage() {
            return resultMessage;
        }

        public void setResultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
        }

        public DataBeanX getData() {
            return data;
        }

        public void setData(DataBeanX data) {
            this.data = data;
        }

        public static class DataBeanX {

            private String id;
            private String uniacid;
            private String acid;
            private String agentid;
            private String spread1;
            private String spread2;
            private String spreadbalance;
            private String sid;
            private String uid;
            private String mall_first_order;
            private String order_type;
            private String is_pay;
            private String ordersn;
            private String order_channel;
            private String serial_sn;
            private String code;
            private String openid;
            private String username;
            private String sex;
            private String mobile;
            private String address;
            private String number;
            private String location_x;
            private String location_y;
            private String note;
            private String price;
            private String box_price;
            private String num;
            private String delivery_day;
            private String delivery_time;
            private String pay_type;
            private String addtime;
            private String paytime;
            private String delivery_handle_type;
            private String delivery_success_location_x;
            private String delivery_success_location_y;
            private String delivery_assign_time;
            private String delivery_instore_time;
            private String delivery_takegoods_time;
            private String delivery_success_time;
            private String handletime;
            private String clerk_notify_collect_time;
            private String endtime;
            private String is_timeout;
            private String status;
            private String refund_status;
            private String delivery_status;
            private String delivery_type;
            private String is_comment;
            private String print_nums;
            private String distance;
            private String delivery_fee;
            private String pack_fee;
            private String serve_fee;
            private String extra_fee;
            private String discount_fee;
            private String total_fee;
            private String final_fee;
            private String vip_free_delivery_fee;
            private String store_final_fee;
            private String store_discount_fee;
            private String plateform_discount_fee;
            private PlateformServeBean plateform_serve;
            private String plateform_serve_rate;
            private String plateform_serve_fee;
            private String plateform_delivery_fee;
            private String plateform_deliveryer_fee;
            private AgentServeBean agent_serve;
            private String agent_final_fee;
            private String agent_serve_fee;
            private String agent_discount_fee;
            private String refund_fee;
            private String invoice;
            private DataBean data;
            private String is_remind;
            private String deliveryer_id;
            private String person_num;
            private String table_id;
            private String table_cid;
            private String reserve_type;
            private String reserve_time;
            private String transaction_id;
            private String out_trade_no;
            private String print_sn;
            private String stat_year;
            private String stat_month;
            private String stat_day;
            private String stat_week;
            private String meals_cn;
            private String last_notify_deliveryer_time;
            private String last_notify_clerk_time;
            private String notify_deliveryer_total;
            private String notify_clerk_total;
            private String elemeOrderId;
            private String elemeDowngraded;
            private String eleme_store_final_fee;
            private String meituanOrderId;
            private String meituan_store_final_fee;
            private String order_plateform;
            private String is_delete;
            private String delivery_collect_type;
            private String transfer_deliveryer_id;
            private String transfer_delivery_status;
            private String delivery_title;
            private String order_type_cn;
            private String status_cn;
            private String pay_type_cn;
            private String pay_type_class;
            private StoreBean store;
            private List<DeliveryersBean> deliveryers;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUniacid() {
                return uniacid;
            }

            public void setUniacid(String uniacid) {
                this.uniacid = uniacid;
            }

            public String getAcid() {
                return acid;
            }

            public void setAcid(String acid) {
                this.acid = acid;
            }

            public String getAgentid() {
                return agentid;
            }

            public void setAgentid(String agentid) {
                this.agentid = agentid;
            }

            public String getSpread1() {
                return spread1;
            }

            public void setSpread1(String spread1) {
                this.spread1 = spread1;
            }

            public String getSpread2() {
                return spread2;
            }

            public void setSpread2(String spread2) {
                this.spread2 = spread2;
            }

            public String getSpreadbalance() {
                return spreadbalance;
            }

            public void setSpreadbalance(String spreadbalance) {
                this.spreadbalance = spreadbalance;
            }

            public String getSid() {
                return sid;
            }

            public void setSid(String sid) {
                this.sid = sid;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getMall_first_order() {
                return mall_first_order;
            }

            public void setMall_first_order(String mall_first_order) {
                this.mall_first_order = mall_first_order;
            }

            public String getOrder_type() {
                return order_type;
            }

            public void setOrder_type(String order_type) {
                this.order_type = order_type;
            }

            public String getIs_pay() {
                return is_pay;
            }

            public void setIs_pay(String is_pay) {
                this.is_pay = is_pay;
            }

            public String getOrdersn() {
                return ordersn;
            }

            public void setOrdersn(String ordersn) {
                this.ordersn = ordersn;
            }

            public String getOrder_channel() {
                return order_channel;
            }

            public void setOrder_channel(String order_channel) {
                this.order_channel = order_channel;
            }

            public String getSerial_sn() {
                return serial_sn;
            }

            public void setSerial_sn(String serial_sn) {
                this.serial_sn = serial_sn;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getOpenid() {
                return openid;
            }

            public void setOpenid(String openid) {
                this.openid = openid;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getSex() {
                return sex;
            }

            public void setSex(String sex) {
                this.sex = sex;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getNumber() {
                return number;
            }

            public void setNumber(String number) {
                this.number = number;
            }

            public String getLocation_x() {
                return location_x;
            }

            public void setLocation_x(String location_x) {
                this.location_x = location_x;
            }

            public String getLocation_y() {
                return location_y;
            }

            public void setLocation_y(String location_y) {
                this.location_y = location_y;
            }

            public String getNote() {
                return note;
            }

            public void setNote(String note) {
                this.note = note;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getBox_price() {
                return box_price;
            }

            public void setBox_price(String box_price) {
                this.box_price = box_price;
            }

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }

            public String getDelivery_day() {
                return delivery_day;
            }

            public void setDelivery_day(String delivery_day) {
                this.delivery_day = delivery_day;
            }

            public String getDelivery_time() {
                return delivery_time;
            }

            public void setDelivery_time(String delivery_time) {
                this.delivery_time = delivery_time;
            }

            public String getPay_type() {
                return pay_type;
            }

            public void setPay_type(String pay_type) {
                this.pay_type = pay_type;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getPaytime() {
                return paytime;
            }

            public void setPaytime(String paytime) {
                this.paytime = paytime;
            }

            public String getDelivery_handle_type() {
                return delivery_handle_type;
            }

            public void setDelivery_handle_type(String delivery_handle_type) {
                this.delivery_handle_type = delivery_handle_type;
            }

            public String getDelivery_success_location_x() {
                return delivery_success_location_x;
            }

            public void setDelivery_success_location_x(String delivery_success_location_x) {
                this.delivery_success_location_x = delivery_success_location_x;
            }

            public String getDelivery_success_location_y() {
                return delivery_success_location_y;
            }

            public void setDelivery_success_location_y(String delivery_success_location_y) {
                this.delivery_success_location_y = delivery_success_location_y;
            }

            public String getDelivery_assign_time() {
                return delivery_assign_time;
            }

            public void setDelivery_assign_time(String delivery_assign_time) {
                this.delivery_assign_time = delivery_assign_time;
            }

            public String getDelivery_instore_time() {
                return delivery_instore_time;
            }

            public void setDelivery_instore_time(String delivery_instore_time) {
                this.delivery_instore_time = delivery_instore_time;
            }

            public String getDelivery_takegoods_time() {
                return delivery_takegoods_time;
            }

            public void setDelivery_takegoods_time(String delivery_takegoods_time) {
                this.delivery_takegoods_time = delivery_takegoods_time;
            }

            public String getDelivery_success_time() {
                return delivery_success_time;
            }

            public void setDelivery_success_time(String delivery_success_time) {
                this.delivery_success_time = delivery_success_time;
            }

            public String getHandletime() {
                return handletime;
            }

            public void setHandletime(String handletime) {
                this.handletime = handletime;
            }

            public String getClerk_notify_collect_time() {
                return clerk_notify_collect_time;
            }

            public void setClerk_notify_collect_time(String clerk_notify_collect_time) {
                this.clerk_notify_collect_time = clerk_notify_collect_time;
            }

            public String getEndtime() {
                return endtime;
            }

            public void setEndtime(String endtime) {
                this.endtime = endtime;
            }

            public String getIs_timeout() {
                return is_timeout;
            }

            public void setIs_timeout(String is_timeout) {
                this.is_timeout = is_timeout;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRefund_status() {
                return refund_status;
            }

            public void setRefund_status(String refund_status) {
                this.refund_status = refund_status;
            }

            public String getDelivery_status() {
                return delivery_status;
            }

            public void setDelivery_status(String delivery_status) {
                this.delivery_status = delivery_status;
            }

            public String getDelivery_type() {
                return delivery_type;
            }

            public void setDelivery_type(String delivery_type) {
                this.delivery_type = delivery_type;
            }

            public String getIs_comment() {
                return is_comment;
            }

            public void setIs_comment(String is_comment) {
                this.is_comment = is_comment;
            }

            public String getPrint_nums() {
                return print_nums;
            }

            public void setPrint_nums(String print_nums) {
                this.print_nums = print_nums;
            }

            public String getDistance() {
                return distance;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public String getDelivery_fee() {
                return delivery_fee;
            }

            public void setDelivery_fee(String delivery_fee) {
                this.delivery_fee = delivery_fee;
            }

            public String getPack_fee() {
                return pack_fee;
            }

            public void setPack_fee(String pack_fee) {
                this.pack_fee = pack_fee;
            }

            public String getServe_fee() {
                return serve_fee;
            }

            public void setServe_fee(String serve_fee) {
                this.serve_fee = serve_fee;
            }

            public String getExtra_fee() {
                return extra_fee;
            }

            public void setExtra_fee(String extra_fee) {
                this.extra_fee = extra_fee;
            }

            public String getDiscount_fee() {
                return discount_fee;
            }

            public void setDiscount_fee(String discount_fee) {
                this.discount_fee = discount_fee;
            }

            public String getTotal_fee() {
                return total_fee;
            }

            public void setTotal_fee(String total_fee) {
                this.total_fee = total_fee;
            }

            public String getFinal_fee() {
                return final_fee;
            }

            public void setFinal_fee(String final_fee) {
                this.final_fee = final_fee;
            }

            public String getVip_free_delivery_fee() {
                return vip_free_delivery_fee;
            }

            public void setVip_free_delivery_fee(String vip_free_delivery_fee) {
                this.vip_free_delivery_fee = vip_free_delivery_fee;
            }

            public String getStore_final_fee() {
                return store_final_fee;
            }

            public void setStore_final_fee(String store_final_fee) {
                this.store_final_fee = store_final_fee;
            }

            public String getStore_discount_fee() {
                return store_discount_fee;
            }

            public void setStore_discount_fee(String store_discount_fee) {
                this.store_discount_fee = store_discount_fee;
            }

            public String getPlateform_discount_fee() {
                return plateform_discount_fee;
            }

            public void setPlateform_discount_fee(String plateform_discount_fee) {
                this.plateform_discount_fee = plateform_discount_fee;
            }

            public PlateformServeBean getPlateform_serve() {
                return plateform_serve;
            }

            public void setPlateform_serve(PlateformServeBean plateform_serve) {
                this.plateform_serve = plateform_serve;
            }

            public String getPlateform_serve_rate() {
                return plateform_serve_rate;
            }

            public void setPlateform_serve_rate(String plateform_serve_rate) {
                this.plateform_serve_rate = plateform_serve_rate;
            }

            public String getPlateform_serve_fee() {
                return plateform_serve_fee;
            }

            public void setPlateform_serve_fee(String plateform_serve_fee) {
                this.plateform_serve_fee = plateform_serve_fee;
            }

            public String getPlateform_delivery_fee() {
                return plateform_delivery_fee;
            }

            public void setPlateform_delivery_fee(String plateform_delivery_fee) {
                this.plateform_delivery_fee = plateform_delivery_fee;
            }

            public String getPlateform_deliveryer_fee() {
                return plateform_deliveryer_fee;
            }

            public void setPlateform_deliveryer_fee(String plateform_deliveryer_fee) {
                this.plateform_deliveryer_fee = plateform_deliveryer_fee;
            }

            public AgentServeBean getAgent_serve() {
                return agent_serve;
            }

            public void setAgent_serve(AgentServeBean agent_serve) {
                this.agent_serve = agent_serve;
            }

            public String getAgent_final_fee() {
                return agent_final_fee;
            }

            public void setAgent_final_fee(String agent_final_fee) {
                this.agent_final_fee = agent_final_fee;
            }

            public String getAgent_serve_fee() {
                return agent_serve_fee;
            }

            public void setAgent_serve_fee(String agent_serve_fee) {
                this.agent_serve_fee = agent_serve_fee;
            }

            public String getAgent_discount_fee() {
                return agent_discount_fee;
            }

            public void setAgent_discount_fee(String agent_discount_fee) {
                this.agent_discount_fee = agent_discount_fee;
            }

            public String getRefund_fee() {
                return refund_fee;
            }

            public void setRefund_fee(String refund_fee) {
                this.refund_fee = refund_fee;
            }

            public String getInvoice() {
                return invoice;
            }

            public void setInvoice(String invoice) {
                this.invoice = invoice;
            }

            public DataBean getData() {
                return data;
            }

            public void setData(DataBean data) {
                this.data = data;
            }

            public String getIs_remind() {
                return is_remind;
            }

            public void setIs_remind(String is_remind) {
                this.is_remind = is_remind;
            }

            public String getDeliveryer_id() {
                return deliveryer_id;
            }

            public void setDeliveryer_id(String deliveryer_id) {
                this.deliveryer_id = deliveryer_id;
            }

            public String getPerson_num() {
                return person_num;
            }

            public void setPerson_num(String person_num) {
                this.person_num = person_num;
            }

            public String getTable_id() {
                return table_id;
            }

            public void setTable_id(String table_id) {
                this.table_id = table_id;
            }

            public String getTable_cid() {
                return table_cid;
            }

            public void setTable_cid(String table_cid) {
                this.table_cid = table_cid;
            }

            public String getReserve_type() {
                return reserve_type;
            }

            public void setReserve_type(String reserve_type) {
                this.reserve_type = reserve_type;
            }

            public String getReserve_time() {
                return reserve_time;
            }

            public void setReserve_time(String reserve_time) {
                this.reserve_time = reserve_time;
            }

            public String getTransaction_id() {
                return transaction_id;
            }

            public void setTransaction_id(String transaction_id) {
                this.transaction_id = transaction_id;
            }

            public String getOut_trade_no() {
                return out_trade_no;
            }

            public void setOut_trade_no(String out_trade_no) {
                this.out_trade_no = out_trade_no;
            }

            public String getPrint_sn() {
                return print_sn;
            }

            public void setPrint_sn(String print_sn) {
                this.print_sn = print_sn;
            }

            public String getStat_year() {
                return stat_year;
            }

            public void setStat_year(String stat_year) {
                this.stat_year = stat_year;
            }

            public String getStat_month() {
                return stat_month;
            }

            public void setStat_month(String stat_month) {
                this.stat_month = stat_month;
            }

            public String getStat_day() {
                return stat_day;
            }

            public void setStat_day(String stat_day) {
                this.stat_day = stat_day;
            }

            public String getStat_week() {
                return stat_week;
            }

            public void setStat_week(String stat_week) {
                this.stat_week = stat_week;
            }

            public String getMeals_cn() {
                return meals_cn;
            }

            public void setMeals_cn(String meals_cn) {
                this.meals_cn = meals_cn;
            }

            public String getLast_notify_deliveryer_time() {
                return last_notify_deliveryer_time;
            }

            public void setLast_notify_deliveryer_time(String last_notify_deliveryer_time) {
                this.last_notify_deliveryer_time = last_notify_deliveryer_time;
            }

            public String getLast_notify_clerk_time() {
                return last_notify_clerk_time;
            }

            public void setLast_notify_clerk_time(String last_notify_clerk_time) {
                this.last_notify_clerk_time = last_notify_clerk_time;
            }

            public String getNotify_deliveryer_total() {
                return notify_deliveryer_total;
            }

            public void setNotify_deliveryer_total(String notify_deliveryer_total) {
                this.notify_deliveryer_total = notify_deliveryer_total;
            }

            public String getNotify_clerk_total() {
                return notify_clerk_total;
            }

            public void setNotify_clerk_total(String notify_clerk_total) {
                this.notify_clerk_total = notify_clerk_total;
            }

            public String getElemeOrderId() {
                return elemeOrderId;
            }

            public void setElemeOrderId(String elemeOrderId) {
                this.elemeOrderId = elemeOrderId;
            }

            public String getElemeDowngraded() {
                return elemeDowngraded;
            }

            public void setElemeDowngraded(String elemeDowngraded) {
                this.elemeDowngraded = elemeDowngraded;
            }

            public String getEleme_store_final_fee() {
                return eleme_store_final_fee;
            }

            public void setEleme_store_final_fee(String eleme_store_final_fee) {
                this.eleme_store_final_fee = eleme_store_final_fee;
            }

            public String getMeituanOrderId() {
                return meituanOrderId;
            }

            public void setMeituanOrderId(String meituanOrderId) {
                this.meituanOrderId = meituanOrderId;
            }

            public String getMeituan_store_final_fee() {
                return meituan_store_final_fee;
            }

            public void setMeituan_store_final_fee(String meituan_store_final_fee) {
                this.meituan_store_final_fee = meituan_store_final_fee;
            }

            public String getOrder_plateform() {
                return order_plateform;
            }

            public void setOrder_plateform(String order_plateform) {
                this.order_plateform = order_plateform;
            }

            public String getIs_delete() {
                return is_delete;
            }

            public void setIs_delete(String is_delete) {
                this.is_delete = is_delete;
            }

            public String getDelivery_collect_type() {
                return delivery_collect_type;
            }

            public void setDelivery_collect_type(String delivery_collect_type) {
                this.delivery_collect_type = delivery_collect_type;
            }

            public String getTransfer_deliveryer_id() {
                return transfer_deliveryer_id;
            }

            public void setTransfer_deliveryer_id(String transfer_deliveryer_id) {
                this.transfer_deliveryer_id = transfer_deliveryer_id;
            }

            public String getTransfer_delivery_status() {
                return transfer_delivery_status;
            }

            public void setTransfer_delivery_status(String transfer_delivery_status) {
                this.transfer_delivery_status = transfer_delivery_status;
            }

            public String getDelivery_title() {
                return delivery_title;
            }

            public void setDelivery_title(String delivery_title) {
                this.delivery_title = delivery_title;
            }

            public String getOrder_type_cn() {
                return order_type_cn;
            }

            public void setOrder_type_cn(String order_type_cn) {
                this.order_type_cn = order_type_cn;
            }

            public String getStatus_cn() {
                return status_cn;
            }

            public void setStatus_cn(String status_cn) {
                this.status_cn = status_cn;
            }

            public String getPay_type_cn() {
                return pay_type_cn;
            }

            public void setPay_type_cn(String pay_type_cn) {
                this.pay_type_cn = pay_type_cn;
            }

            public String getPay_type_class() {
                return pay_type_class;
            }

            public void setPay_type_class(String pay_type_class) {
                this.pay_type_class = pay_type_class;
            }

            public StoreBean getStore() {
                return store;
            }

            public void setStore(StoreBean store) {
                this.store = store;
            }

            public List<DeliveryersBean> getDeliveryers() {
                return deliveryers;
            }

            public void setDeliveryers(List<DeliveryersBean> deliveryers) {
                this.deliveryers = deliveryers;
            }

            public static class PlateformServeBean {

                private int fee_type;
                private int fee_rate;
                private double fee;
                private String note;

                public int getFee_type() {
                    return fee_type;
                }

                public void setFee_type(int fee_type) {
                    this.fee_type = fee_type;
                }

                public int getFee_rate() {
                    return fee_rate;
                }

                public void setFee_rate(int fee_rate) {
                    this.fee_rate = fee_rate;
                }

                public double getFee() {
                    return fee;
                }

                public void setFee(double fee) {
                    this.fee = fee;
                }

                public String getNote() {
                    return note;
                }

                public void setNote(String note) {
                    this.note = note;
                }
            }

            public static class AgentServeBean {

                @SerializedName("final")
                private String finalX;

                public String getFinalX() {
                    return finalX;
                }

                public void setFinalX(String finalX) {
                    this.finalX = finalX;
                }
            }

            public static class DataBean {

                private CartBean cart;
                private CommissionBean commission;
                private List<?> extra_fee;

                public CartBean getCart() {
                    return cart;
                }

                public void setCart(CartBean cart) {
                    this.cart = cart;
                }

                public CommissionBean getCommission() {
                    return commission;
                }

                public void setCommission(CommissionBean commission) {
                    this.commission = commission;
                }

                public List<?> getExtra_fee() {
                    return extra_fee;
                }

                public void setExtra_fee(List<?> extra_fee) {
                    this.extra_fee = extra_fee;
                }

                public static class CartBean {

                    @SerializedName("11731")
                    private _$11731Bean _$11731;

                    public _$11731Bean get_$11731() {
                        return _$11731;
                    }

                    public void set_$11731(_$11731Bean _$11731) {
                        this._$11731 = _$11731;
                    }

                    public static class _$11731Bean {

                        private String goods_id;
                        private String title;
                        private List<OptionsBean> options;

                        public String getGoods_id() {
                            return goods_id;
                        }

                        public void setGoods_id(String goods_id) {
                            this.goods_id = goods_id;
                        }

                        public String getTitle() {
                            return title;
                        }

                        public void setTitle(String title) {
                            this.title = title;
                        }

                        public List<OptionsBean> getOptions() {
                            return options;
                        }

                        public void setOptions(List<OptionsBean> options) {
                            this.options = options;
                        }

                        public static class OptionsBean {

                            private String option_id;
                            private String name;
                            private int num;
                            private int price_num;
                            private int discount_num;
                            private int bargain_id;
                            private String price_total;

                            public String getOption_id() {
                                return option_id;
                            }

                            public void setOption_id(String option_id) {
                                this.option_id = option_id;
                            }

                            public String getName() {
                                return name;
                            }

                            public void setName(String name) {
                                this.name = name;
                            }

                            public int getNum() {
                                return num;
                            }

                            public void setNum(int num) {
                                this.num = num;
                            }

                            public int getPrice_num() {
                                return price_num;
                            }

                            public void setPrice_num(int price_num) {
                                this.price_num = price_num;
                            }

                            public int getDiscount_num() {
                                return discount_num;
                            }

                            public void setDiscount_num(int discount_num) {
                                this.discount_num = discount_num;
                            }

                            public int getBargain_id() {
                                return bargain_id;
                            }

                            public void setBargain_id(int bargain_id) {
                                this.bargain_id = bargain_id;
                            }

                            public String getPrice_total() {
                                return price_total;
                            }

                            public void setPrice_total(String price_total) {
                                this.price_total = price_total;
                            }
                        }
                    }
                }

                public static class CommissionBean {

                    private String spread1_rate;
                    private int spread1;
                    private String spread2_rate;
                    private int spread2;

                    public String getSpread1_rate() {
                        return spread1_rate;
                    }

                    public void setSpread1_rate(String spread1_rate) {
                        this.spread1_rate = spread1_rate;
                    }

                    public int getSpread1() {
                        return spread1;
                    }

                    public void setSpread1(int spread1) {
                        this.spread1 = spread1;
                    }

                    public String getSpread2_rate() {
                        return spread2_rate;
                    }

                    public void setSpread2_rate(String spread2_rate) {
                        this.spread2_rate = spread2_rate;
                    }

                    public int getSpread2() {
                        return spread2;
                    }

                    public void setSpread2(int spread2) {
                        this.spread2 = spread2;
                    }
                }
            }

            public static class StoreBean {

                private String location_x;
                private String location_y;

                public String getLocation_x() {
                    return location_x;
                }

                public void setLocation_x(String location_x) {
                    this.location_x = location_x;
                }

                public String getLocation_y() {
                    return location_y;
                }

                public void setLocation_y(String location_y) {
                    this.location_y = location_y;
                }
            }

            public static class DeliveryersBean {

                private String id;
                private String uniacid;
                private String agentid;
                private String title;
                private String nickname;
                private String groupid;
                private String openid;
                private String openid_wxapp;
                private String avatar;
                private String mobile;
                private String password;
                private String salt;
                private String token;
                private String sex;
                private String age;
                private String addtime;
                private String credit1;
                private String credit2;
                private String work_status;
                private String is_takeout;
                private String is_errander;
                private String auth_info;
                private String location_x;
                private String location_y;
                private String order_takeout_num;
                private String order_errander_num;
                private String collect_max_takeout;
                private String collect_max_errander;
                private String perm_transfer;
                private String perm_cancel;
                private String fee_delivery;
                private String fee_getcash;
                private ExtraBean extra;
                private Object registration_id;
                private int order_id;
                private String store2user_distance;
                private String store2deliveryer_distance;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getUniacid() {
                    return uniacid;
                }

                public void setUniacid(String uniacid) {
                    this.uniacid = uniacid;
                }

                public String getAgentid() {
                    return agentid;
                }

                public void setAgentid(String agentid) {
                    this.agentid = agentid;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }

                public String getGroupid() {
                    return groupid;
                }

                public void setGroupid(String groupid) {
                    this.groupid = groupid;
                }

                public String getOpenid() {
                    return openid;
                }

                public void setOpenid(String openid) {
                    this.openid = openid;
                }

                public String getOpenid_wxapp() {
                    return openid_wxapp;
                }

                public void setOpenid_wxapp(String openid_wxapp) {
                    this.openid_wxapp = openid_wxapp;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getMobile() {
                    return mobile;
                }

                public void setMobile(String mobile) {
                    this.mobile = mobile;
                }

                public String getPassword() {
                    return password;
                }

                public void setPassword(String password) {
                    this.password = password;
                }

                public String getSalt() {
                    return salt;
                }

                public void setSalt(String salt) {
                    this.salt = salt;
                }

                public String getToken() {
                    return token;
                }

                public void setToken(String token) {
                    this.token = token;
                }

                public String getSex() {
                    return sex;
                }

                public void setSex(String sex) {
                    this.sex = sex;
                }

                public String getAge() {
                    return age;
                }

                public void setAge(String age) {
                    this.age = age;
                }

                public String getAddtime() {
                    return addtime;
                }

                public void setAddtime(String addtime) {
                    this.addtime = addtime;
                }

                public String getCredit1() {
                    return credit1;
                }

                public void setCredit1(String credit1) {
                    this.credit1 = credit1;
                }

                public String getCredit2() {
                    return credit2;
                }

                public void setCredit2(String credit2) {
                    this.credit2 = credit2;
                }

                public String getWork_status() {
                    return work_status;
                }

                public void setWork_status(String work_status) {
                    this.work_status = work_status;
                }

                public String getIs_takeout() {
                    return is_takeout;
                }

                public void setIs_takeout(String is_takeout) {
                    this.is_takeout = is_takeout;
                }

                public String getIs_errander() {
                    return is_errander;
                }

                public void setIs_errander(String is_errander) {
                    this.is_errander = is_errander;
                }

                public String getAuth_info() {
                    return auth_info;
                }

                public void setAuth_info(String auth_info) {
                    this.auth_info = auth_info;
                }

                public String getLocation_x() {
                    return location_x;
                }

                public void setLocation_x(String location_x) {
                    this.location_x = location_x;
                }

                public String getLocation_y() {
                    return location_y;
                }

                public void setLocation_y(String location_y) {
                    this.location_y = location_y;
                }

                public String getOrder_takeout_num() {
                    return order_takeout_num;
                }

                public void setOrder_takeout_num(String order_takeout_num) {
                    this.order_takeout_num = order_takeout_num;
                }

                public String getOrder_errander_num() {
                    return order_errander_num;
                }

                public void setOrder_errander_num(String order_errander_num) {
                    this.order_errander_num = order_errander_num;
                }

                public String getCollect_max_takeout() {
                    return collect_max_takeout;
                }

                public void setCollect_max_takeout(String collect_max_takeout) {
                    this.collect_max_takeout = collect_max_takeout;
                }

                public String getCollect_max_errander() {
                    return collect_max_errander;
                }

                public void setCollect_max_errander(String collect_max_errander) {
                    this.collect_max_errander = collect_max_errander;
                }

                public String getPerm_transfer() {
                    return perm_transfer;
                }

                public void setPerm_transfer(String perm_transfer) {
                    this.perm_transfer = perm_transfer;
                }

                public String getPerm_cancel() {
                    return perm_cancel;
                }

                public void setPerm_cancel(String perm_cancel) {
                    this.perm_cancel = perm_cancel;
                }

                public String getFee_delivery() {
                    return fee_delivery;
                }

                public void setFee_delivery(String fee_delivery) {
                    this.fee_delivery = fee_delivery;
                }

                public String getFee_getcash() {
                    return fee_getcash;
                }

                public void setFee_getcash(String fee_getcash) {
                    this.fee_getcash = fee_getcash;
                }

                public ExtraBean getExtra() {
                    return extra;
                }

                public void setExtra(ExtraBean extra) {
                    this.extra = extra;
                }

                public Object getRegistration_id() {
                    return registration_id;
                }

                public void setRegistration_id(Object registration_id) {
                    this.registration_id = registration_id;
                }

                public int getOrder_id() {
                    return order_id;
                }

                public void setOrder_id(int order_id) {
                    this.order_id = order_id;
                }

                public String getStore2user_distance() {
                    return store2user_distance;
                }

                public void setStore2user_distance(String store2user_distance) {
                    this.store2user_distance = store2user_distance;
                }

                public String getStore2deliveryer_distance() {
                    return store2deliveryer_distance;
                }

                public void setStore2deliveryer_distance(String store2deliveryer_distance) {
                    this.store2deliveryer_distance = store2deliveryer_distance;
                }

                public static class ExtraBean {

                    private int accept_wechat_notice;
                    private int accept_voice_notice;

                    public int getAccept_wechat_notice() {
                        return accept_wechat_notice;
                    }

                    public void setAccept_wechat_notice(int accept_wechat_notice) {
                        this.accept_wechat_notice = accept_wechat_notice;
                    }

                    public int getAccept_voice_notice() {
                        return accept_voice_notice;
                    }

                    public void setAccept_voice_notice(int accept_voice_notice) {
                        this.accept_voice_notice = accept_voice_notice;
                    }
                }
            }
        }
    }
}
