package com.lalawaimai.palteform.baselib.icon;

import com.joanzapata.iconify.Icon;

/**
 * 图标字体库的枚举类，所有图标都要在这个类中进行添加
 * Created by WaterWood on 2018/6/4.
 */
public enum  EcIcons implements Icon {

    icon_add('\ue6cf'),//加号
    icon_classes('\ue602'),//分类
    icon_magnifier('\ue60e'),//放大镜
    icon_location('\ue502'),//定位
    icon_phone('\ue60d');//电话

    private char character;

    EcIcons(char character) {
        this.character = character;
    }

    @Override
    public String key() {
        return name().replace("_","-");
    }

    @Override
    public char character() {
        return character;
    }
}
