package com.lalawaimai.plateform.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.lalawaimai.palteform.baselib.base.BaseCommonActivity;
import com.lalawaimai.palteform.baselib.http.CommonBean;
import com.lalawaimai.palteform.baselib.http.DisposeDataListener;
import com.lalawaimai.palteform.baselib.http.OkHttpException;
import com.lalawaimai.palteform.baselib.http.RequestWebInfo;
import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.LoginBean;
import com.lalawaimai.plateform.bean.SerialNumberBean;
import com.lalawaimai.plateform.constant.Constant;
import com.lalawaimai.plateform.home.HomeActivity;
import com.lalawaimai.plateform.http.WebUrl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;

/**
 * 登录界面
 * Created by WaterWood on 2018/6/7.
 */
public class LoginActivity extends BaseCommonActivity implements View.OnClickListener{

    private View status_bar;
    private LinearLayout ll_back;
    private TextView tv_title;
    private EditText login_name;
    private EditText login_key;
    private Button bt_commit;
    private ProgressDialog dialogWeb;//网络加载等待框

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initWidget() {
        status_bar = findViewById(R.id.status_bar);
        ll_back = findViewById(R.id.ll_back);
        tv_title = findViewById(R.id.tv_title);
        setStatusBarSize(this,status_bar);
        ll_back.setVisibility(View.GONE);
        tv_title.setText("登录");
        login_name = findViewById(R.id.login_name);
        login_key = findViewById(R.id.login_key);
        bt_commit = findViewById(R.id.bt_commit);
        bt_commit.setOnClickListener(this);
        dialogWeb = CommonUtil.showProgressDialog(this, "正在加载");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_commit:
                login();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            return true;
        }else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void login(){
        String loginName = login_name.getText().toString().trim();
        String loginKey = login_key.getText().toString().trim();
        if (!NullUtil.isStringEmpty(loginName) && !NullUtil.isStringEmpty(loginKey)){
            //登录
            dialogWeb.show();
            String url = WebUrl.LOGIN;
            Class<?> clazz = LoginBean.class;
            final List<String> list = new ArrayList<>();
            list.add("username");
            list.add("password");
            list.add("registration_id");
            JPushInterface.getRegistrationID(this);
            new RequestWebInfo(new DisposeDataListener() {
                @Override
                public void onSuccess(Object responseObj) {
                    dialogWeb.dismiss();
                    LoginBean loginBean = (LoginBean) responseObj;
                    CommonUtil.saveData(LoginActivity.this, Constant.TOKEN,loginBean.getMessage().getData().getToken(),Constant.STRING);
                    //写入别名和标签
                    JPushInterface.setAlias(LoginActivity.this,1001,loginBean.getMessage().getData().getJpush_relation().getAlias());
                    Set<String> tags = new HashSet<>();
                    for (int i=0;i<loginBean.getMessage().getData().getJpush_relation().getTags().size();i++){
                        tags.add(loginBean.getMessage().getData().getJpush_relation().getTags().get(i));
                    }
                    JPushInterface.setTags(LoginActivity.this,1001,tags);
                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(Object reasonObj) {
                    dialogWeb.dismiss();
                    OkHttpException okHttpException = (OkHttpException) reasonObj;
                    String msg = okHttpException.getEmsg().toString();
                    if (msg.contains("java.net.ConnectException")) {
                        CommonUtil.showToast(LoginActivity.this, "无法连接服务器，请检查您的网络");
                    } else {
                        CommonUtil.showToast(LoginActivity.this, msg);
                    }
                }
            }).requestWeb(url, clazz, list, loginName,loginKey,JPushInterface.getRegistrationID(this));
        }else if (NullUtil.isStringEmpty(loginName)){
            CommonUtil.showToast(this,"请输入账号");
        }else if (NullUtil.isStringEmpty(loginKey)){
            CommonUtil.showToast(this,"请输入密码");
        }
    }
}
