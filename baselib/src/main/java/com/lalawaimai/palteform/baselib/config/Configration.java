package com.lalawaimai.palteform.baselib.config;

import android.net.NetworkRequest;

import com.joanzapata.iconify.Iconify;
import com.lalawaimai.palteform.baselib.icon.FontPFMoudle;

import okhttp3.internal.http.HttpEngine;

/**
 * 初始化参数配置类
 */
public class Configration {

    private static Configration configration;

    /**
     * 获取对象实例
     *
     * @return
     */
    public static Configration getInstance() {
        if (configration == null) {
            configration = new Configration();
        }
        return configration;
    }

    /**
     * 图标库相关初始化
     */
    public void initIconify(){
        Iconify.with(new FontPFMoudle());
    }
}
