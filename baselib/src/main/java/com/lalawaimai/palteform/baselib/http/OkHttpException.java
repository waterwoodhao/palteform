package com.lalawaimai.palteform.baselib.http;

/**
 * 异常处理类
 * Created by WaterWood on 2018/5/29.
 */
public class OkHttpException extends Exception {
    private static final long serialVersionUID = 1L;
    private int ecode;//异常码
    private Object emsg;//异常信息

    public OkHttpException(int ecode, Object emsg) {
        this.ecode = ecode;
        this.emsg = emsg;
    }

    public int getEcode() {
        return ecode;
    }

    public Object getEmsg() {
        return emsg;
    }
}
