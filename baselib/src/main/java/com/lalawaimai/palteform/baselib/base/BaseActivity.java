package com.lalawaimai.palteform.baselib.base;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;


import com.lalawaimai.palteform.baselib.utils.CommonUtil;

import java.util.List;

import qiu.niorgai.StatusBarCompat;

/**
 * Activity基类
 * Created by WaterWood on 2018/5/9.
 */
public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //在界面未初始化之前调用的初始化窗口
        initWindows();
        if (initArgs(getIntent().getExtras())) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(getContentLayoutId());
            setStatusColor();
            initWidget();
            intiData();
        } else {
            finish();
        }
    }

    /**
     * 初始化窗口
     */
    protected void initWindows() {

    }

    /**
     * 初始化相关参数
     *
     * @param bundle 参数bundle
     * @return 如果参数正确返回true, 错误返回false
     */
    protected boolean initArgs(Bundle bundle) {
        return true;
    }

    /**
     * 得到当前界面的资源文件Id
     *
     * @return 资源文件Id
     */
    protected abstract int getContentLayoutId();

    /**
     * 初始化控件
     */
    protected void initWidget() {

    }

    /**
     * 初始化数据
     */
    protected void intiData() {

    }

    @Override
    public boolean onSupportNavigateUp() {
        //点击当前界面导航返回时，finish当前界面
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        //得到当前Activity下的所有Fragment
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        //判断集合是否为空
        if (fragments != null && fragments.size() > 0) {
            for (Fragment fragment : fragments) {
                //判断是否为我们能够处理的Fragment类型
                if (fragment instanceof BaseFragment) {
                    //是否拦截了返回按钮
                    if (((BaseFragment) fragment).onBackPressed()) {
                        //如果有，直接return
                        return;
                    }
                }
            }
        }
        super.onBackPressed();
        finish();
    }

    /**
     * 设置每个页面的导航栏和下面操作栏的颜色，这个只有19以后才有
     */
    protected void setStatusColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            StatusBarCompat.translucentStatusBar(this, true);
        }
    }

    /**
     * 设置状态栏占位控件的大小，在需要的时候调用
     * @param context
     * @param status_bar
     */
    public void setStatusBarSize(Context context, View status_bar) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, CommonUtil.getStateBarHeight(context));
            status_bar.setLayoutParams(lp);
        } else {
            status_bar.setVisibility(View.GONE);
        }
    }
}
